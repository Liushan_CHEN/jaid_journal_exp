# JAID Experiment 4 Journal Preparation Script #
Since it may cause plenty of time for a user who are not familiar with Defects4J or IntroClassJava to prepare the buggy program and a properties file for JAID, we implement this additional script to help preparing both elements needed. With this script, you can get everything ready by one command.
### Environment requirement ###
* JDK8
* Python3 (with pip3)
* initialized Defects4J

### Getting start ###
1. Clone this project.

2. Just run the script to see the helping message.<br />
``` python3 <PATH_TO_this_project>/src/main.py -h ```

3. Input the JDK8 path of local machine when asked (which will happen only when there is no system environment variable ``JAVA_HOME`` that points to JDK8).

4. Input the root path of Defects4J when asked

### Common Usage Examples ###
* To prepare the a specified Defects4J (Lang33) and IntroClassJava (checksum-e23b9_005) buggy projects, you can execute following command.<br /> 
```$ python3 <PATH_TO_jaid_exp_pre>/src/main.py --pre Lang33,checksum-e23b9_005```<br />
Then the script mention above will download the project and generate the properties file that used by JAID automatically, and print the running argument (including the path of the properties file) that can be used to run JAID.<br />
```--FixjaSettingFile <PATH_TO_jaid_exp_pre>/buggy_repo/commons-lang33/local_fixja.properties```

* Command to see all the available project and buggy version.<br />
```$ python3 <PATH_TO_jaid_exp_pre>/src/main.py --info ```

* Command to run JAID to fix the specified Defects4J and IntroClassJava projects (prepared automatically).<br />
```$ python3 <PATH_TO_jaid_exp_pre>/src/main.py --run Lang33,checksum-e23b9_005```

* Command to read the JAID output of the specified Defects4J and IntroClassJava projects.<br />
```$ python3 <PATH_TO_jaid_exp_pre>/src/main.py --read Lang33,checksum-e23b9_005```