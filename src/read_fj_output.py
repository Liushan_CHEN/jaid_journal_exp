import csv
import os

from constants import ANALYSIS_TMP, FIX_RANKING_SCORE_FILE_NAME
from fj_obj import split_log_line, parse_time, TheResult, TheFixAction, TheSnapshot


def read_new_jaid_result(jaid_output_dir):
    print('JAID_output_path: ' + jaid_output_dir)
    print('###Processing ...')
    result = None
    for exp_output in os.listdir(jaid_output_dir):
        src = os.path.join(jaid_output_dir, exp_output)
        if src.endswith("plausible_fix_actions.log"):
            print('### read plausible_fix_actions ...')
            result = read_plausible_log(src)
            # used_schema_set, used_strategy_set = collect_strategy_and_schema(src)
            # print('Used strategy set:' + ','.join(used_strategy_set))
            # print('Used schema set:' + ','.join(used_schema_set))
            print('Mean of valid fix patch size:' + str(result.fix_line_size))
    if result is not None:
        return result.print_result()


def read_plausible_log(log_path):
    result = TheResult()
    plausible_count = 0
    found = False
    fix_line_size = 0

    with open(log_path, 'r') as file:
        for line in file:
            if result.start_time is None and 'JAID started' in line:
                line_con = split_log_line(line)
                result.start_time = parse_time(line_con[0])
            elif result.monitor_finish_time is None and 'Finish evaluate program states' in line:
                line_con = split_log_line(line)
                result.monitor_finish_time = parse_time(line_con[0])
            elif result.build_snapshot_finish_time is None and 'Finish building snapshots' in line:
                line_con = split_log_line(line)
                result.build_snapshot_finish_time = parse_time(line_con[0])
            elif result.build_fix_finish_time is None and 'Finish building fixes' in line:
                line_con = split_log_line(line)
                result.build_fix_finish_time = parse_time(line_con[0])
            elif result.all_finish_time is None and 'Finished.' in line:
                line_con = split_log_line(line)
                result.all_finish_time = parse_time(line_con[0])
            elif result.finish_validation_time is None and 'Number of valid fix actions' in line:
                line_con = split_log_line(line)
                result.finish_validation_time = parse_time(line_con[0])
            elif result.first_plausible_time is None and found and 'FixAction{fixId' in line:
                line_con = split_log_line(line)
                result.first_plausible_time = parse_time(line_con[0])
                plausible_count += 1
                fix_line_size += get_line_size(line)
                found = False
            elif result.snapshot_size == 0 and 'Valid snapshots' in line:
                line_con = split_log_line(line)
                result.snapshot_size = get_value(line_con[1])
                result.build_snapshot_finish_time = parse_time(line_con[0])
            elif result.etm_size == 0 and 'AllNonSideEffectETM' in line:
                line_con = split_log_line(line)
                result.etm_size = get_value(line_con[1])
            elif result.snippet_size == 0 and 'Valid snippets' in line:
                line_con = split_log_line(line)
                result.snippet_size = get_value(line_con[1])
            elif result.fix_size == 0 and 'Generated fixactions' in line:
                line_con = split_log_line(line)
                result.fix_size = get_value(line_con[1])
            elif result.evaluated_count == 0 and 'Number of fix actions to validate' in line:
                line_con = split_log_line(line)
                result.evaluated_count = get_value(line_con[1])
                result.finish_generation_time = parse_time(line_con[0])
            elif result.location_size == 0 and 'Valid locations' in line:
                line_con = split_log_line(line)
                result.location_size = get_value(line_con[1])
            elif result.ranked_count == 0 and 'Ranked valid fixes' in line:
                line_con = split_log_line(line)
                result.ranked_count = get_value(line_con[1])
                pass  # todo:another function to read and output ranking csv
            elif 'TotalTest' in line:
                line_con = split_log_line(line)
                tests = line_con[1].strip().split(';')
                result.total_test = get_value(tests[0])
                result.valid_test = get_value(tests[1])
                result.passing_test = get_value(tests[2])
            elif found and 'FixAction{fixId' in line:
                plausible_count += 1
                fix_line_size += get_line_size(line)
                found = False
            elif 'valid fix found::' in line:
                found = True
            elif 'ERROR -' in line:
                pass
                # print('ERROR-- ' + line)
        result.plausible_count = plausible_count
        if plausible_count != 0:
            result.fix_line_size = fix_line_size / plausible_count
        return result


def get_line_size(line):
    if 'Schema::SCHEMA_A' in line:
        return 1
    elif 'Schema::SCHEMA_B' in line:
        return 2
    elif 'Schema::SCHEMA_C' in line:
        return 1
    elif 'Schema::SCHEMA_D' in line:
        return 3
    elif 'Schema::SCHEMA_E' in line:
        return 1


def collect_strategy_and_schema(log_path):
    used_strategy = []
    used_schema = []
    all_plausible_fix = get_fixes_from_log(log_path)
    for item in all_plausible_fix:
        used_strategy.append(item.strategy)
        used_schema.append(item.schema)
    return set(used_schema), set(used_strategy)


def count_evaluated_fix(log_path):
    count = 0
    with open(log_path, 'r') as file:
        for line in file:
            if 'FixAction{fixId' in line:
                count += 1
    return count


def get_value(con):
    cons = con.strip().split('::')
    if len(cons) == 2:
        return int(cons[1])


def get_evaluated_fix_rank(log_path):
    fixid_rank_map = {}
    evaluated_fix_list = get_fixes_from_log(log_path)
    for item in evaluated_fix_list:
        idx = evaluated_fix_list.index(item)
        fixid_rank_map[item.fixid] = idx
    return fixid_rank_map


def analyze_ranking(log_path):
    ranked_fix_list = []
    fix_list = get_fixes_from_log(log_path)
    for fix in fix_list:
        if not (fix.location_score == 0 and fix.state_score == 0 and fix.fix_score == 0):
            ranked_fix_list.append(fix)
            # print(fix.get_fix_str())
    path, file = os.path.split(log_path)
    csv_path = os.path.join(path, FIX_RANKING_SCORE_FILE_NAME)
    if os.path.isfile(csv_path):
        os.remove(csv_path)
    with open(csv_path, 'w')as f:
        f_csv = csv.writer(f)
        f_csv.writerow(['fixid', 'location_score', 'state_score', 'fix_score'])
        for fix in ranked_fix_list:
            f_csv.writerow(fix.get_score_row())


def select_desired_fixaction(log_path):
    selected_fix_list = []
    fix_list = get_fixes_from_log(log_path)
    if fix_list:
        location = input('Desired line number (press enter to omit):')
        schema = input('Desired schema (press enter to omit):')
        strategy = input('Desired strategy (press enter to omit):')
        if len(location) > 0 and int(location) > 0 and len(schema) > 0 and len(strategy) > 0:
            for fix in fix_list:
                if int(location) == fix.location and \
                                schema in fix.schema and strategy in fix.strategy:
                    selected_fix_list.append(fix)
        elif len(schema) > 0 and len(strategy) > 0:
            for fix in fix_list:
                if schema in fix.schema and strategy in fix.strategy:
                    selected_fix_list.append(fix)
        elif len(location) > 0 and int(location) > 0 and len(strategy) > 0:
            for fix in fix_list:
                if int(location) == fix.location and strategy in fix.strategy:
                    selected_fix_list.append(fix)
        elif len(location) > 0 and int(location) > 0 and len(schema) > 0:
            for fix in fix_list:
                if int(location) == fix.location and schema in fix.schema:
                    selected_fix_list.append(fix)
        elif len(location) > 0 and int(location) > 0:
            for fix in fix_list:
                if int(location) == fix.location:
                    selected_fix_list.append(fix)
        elif len(schema) > 0:
            for fix in fix_list:
                if schema in fix.schema:
                    selected_fix_list.append(fix)
        elif len(strategy) > 0:
            for fix in fix_list:
                if strategy in fix.strategy:
                    selected_fix_list.append(fix)
        else:
            print('Input condition error. location:' + location + ' schema:' + schema + ' strategy:' + strategy + '.')

    if len(selected_fix_list) > 0:
        selected_result_file_path = os.path.join(os.path.split(log_path)[0], ANALYSIS_TMP)
        print('selected_result_file_path:' + selected_result_file_path)
        with open(selected_result_file_path, 'w') as file:
            file.write('Count:' + str(len(selected_fix_list)))
            for fix in selected_fix_list:
                file.write(fix.get_fix_str())


def get_fixes_from_log(log_path):
    fix_list = []
    start_time = None
    read_fix = False
    fix_action = None
    if os.path.isfile(log_path):
        with open(log_path, 'r') as file:
            for line in file:
                if 'FixAction{fixId' in line:
                    fix_action = TheFixAction(line, start_time)
                    fix_list.append(fix_action)
                    read_fix = True
                elif line.strip() == ']' or line.strip() == ';]':
                    read_fix = False
                elif read_fix and fix_action:
                    fix_action.fix += line
                elif fix_action and ', location=' in line and fix_action.location == 0:
                    location = int(line[11:line.index('::')])
                    fix_action.location = location
                elif start_time is None and 'JAID started' in line:
                    line_con = split_log_line(line)
                    start_time = parse_time(line_con[0])
        return fix_list
    else:
        print(log_path + ' is not a file.')


def select_opposite_snapshot(log_path):
    desired_list = []
    snapshot_list = get_snapshot_from_log(log_path)
    if snapshot_list:
        diff_str = input('Max index diff (press enter to omit):')
        if diff_str and diff_str.isalnum():
            diff = int(diff_str)
        else:
            diff = 10000
        for count_a, snapshot_a in enumerate(snapshot_list):
            for snapshot_b in snapshot_list[count_a + 1:]:
                if snapshot_a.opposite(snapshot_b, diff):
                    desired_list.append(snapshot_a)
                    desired_list.append(snapshot_b)
    print('Opposite snapshot size:' + str(len(desired_list) / 2))
    for snapshot in desired_list:
        print(snapshot)


def get_snapshot_from_log(log_path):
    snapshot_list = []
    if os.path.isfile(log_path):
        with open(log_path, 'r') as file:
            for line in file:
                if 'StateSnapshot{' in line:
                    snapshot = TheSnapshot(line)
                    snapshot_list.append(snapshot)
        return snapshot_list
    else:
        print(log_path + ' is not a file.')


Head = ['BugId', 'start_time', 'total_test', 'valid_test', 'passing_test', 'location_size', 'etm_size', 'snapshot_size',
        'snippet_size', 'fix_size', 'evaluated_count',
        'plausible_count', 'ranked_count', 'monitor_finish_time', 'first_plausible_time',
        'all_finish_time', 'build_snapshot_finish_time', 'generation_costing_time',
        'validation_costing_time', 'ranking_costing_time']


def append_csv(csv_path, csv_row):
    if os.path.isfile(csv_path):
        with open(csv_path, 'a+')as f:
            f_csv = csv.writer(f)
            f_csv.writerow(csv_row)
    else:
        with open(csv_path, 'w')as f:
            f_csv = csv.writer(f)
            f_csv.writerow(Head)
            f_csv.writerow(csv_row)
