import os

import requests

# Download shared file on google drive by file id (which can get from the shared link)

URL = "https://docs.google.com/uc?export=download"


# if the file download from google drive is a BadZipFile and only 65kb, it means that the file is not public shared
def download_file_from_google_drive(id, destination):
    session = requests.Session()
    print('Downloading ' + id + ' to ' + destination)

    response = session.get(URL, params={'id': id}, stream=True)
    token = get_confirm_token(response)

    if token:
        params = {'id': id, 'confirm': token}
        response = session.get(URL, params=params, stream=True)

    token = save_response_content(response, destination)

    if token:
        params = {'id': id, 'confirm': token}
        response = session.get(URL, params=params, stream=True)
        save_response_content(response, destination)


def get_confirm_token(response):
    for key, value in response.cookies.items():
        if key.startswith('download_warning'):
            return value

    return None


def save_response_content(response, destination):
    CHUNK_SIZE = 32768
    with open(destination, "wb") as f:
        for chunk in response.iter_content(CHUNK_SIZE):
            if chunk:  # filter out keep-alive new chunks
                f.write(chunk)
    if os.path.getsize(destination) < 10000:
        with open(destination, "r") as f:
            for line in f.readlines():
                if 'confirm=' in line:
                    token = line[line.indexof('confirm=') + len('confirm='):]
                    if '&amp;' in token:
                        token = token[:token.indexof('&amp;')]
                        return token
    return None

# for can_key in CANDIDATES.keys():
#     download_file_from_google_drive(CANDIDATES[can_key], can_key)
