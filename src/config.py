import os

from jaid_properties import JaidProperties
from utils import get_java8_home

JUNIT_PATH = ['libs', 'junit-4.12.jar']
HAMCREST_PATH = ['libs', 'hamcrest-core-1.3.jar']
WORKING_REL_PATH = 'buggy_repo'
BACKUP_PATH = 'backup'
D4J_FOLDER = 'defects4j'
LIB_FOLDER = 'libs'
PROPERTIES_FOLDER = 'properties_files'
DEV_FIXED_FILES = 'dev_fixed_files'
MODEL_FOLDER = 'models'
JAID_JAR = 'jaid-all.jar'
RESTORE_JAR = 'restore.jar'
D4J_BIN = 'framework/bin/defects4j'


class MyConfig(object):
    """A configInfo object"""
    p_IcjBlackBoxOnly = 'IcjBlackBoxOnly'
    p_D4jExe = 'D4jExe'
    p_BearsExe = 'BearsExe'
    p_Jar = 'UsedJar'
    p_ProcessDevFix = 'ProcessDevFix'
    p_RankModel = 'RankModel'
    p_TwoModels = 'TwoModels'
    p_UpdateModel = 'UpdateModel'
    p_TimeLimit = 'TimeLimit'
    p_ModelPrepare = 'ModelPrepare'
    p_SimfixMode = 'SimfixMode'
    p_SimfixPatchNum = 'SimfixPatchNum'

    def __init__(self):
        super(MyConfig, self).__init__()
        self.thisPath = ''
        self.JDKDir = ''
        self.SbflAlgorithm = ''
        self.RankingAlgorithm = ''
        self.MaxTestNumber = ''
        self.d4j_exe = ''
        self.bears_exe = ''
        self.root = get_script_root_path()
        self.IcjBlackBoxOnly = True
        self.used_jar = ''
        self.process_dev_fix = False
        self.rank_model = ''
        self.two_models = True
        self.update_model = True
        self.time_limit = 600
        self.model_prepare = False
        self.simfix_mode = 'pre'
        self.patch_num = 1

    def __str__(self):
        return "JDKDir: %s\n" \
               "d4j_exe: %s\n " \
               "bears_exe: %s\n " \
               "SbflAlgorithm = %s\n " \
               "RankingAlgorithm =%s\n " \
               "MaxTestNumber =%s\n " \
               "IcjBlackBoxOnly =%s\n " \
               "used_jar =%s\n " \
               "process_dev_fix =%s\n " \
               "rank_model =%s\n " \
               "two_models =%s\n " \
               "update_model =%s\n " \
               "time_limit =%s\n " \
               "model_prepare =%s\n " \
               "simfix_mode =%s\n " \
               "patch_num =%s\n " \
               % (self.JDKDir,
                  self.d4j_exe,
                  self.bears_exe,
                  self.SbflAlgorithm,
                  self.RankingAlgorithm,
                  self.MaxTestNumber,
                  str(self.IcjBlackBoxOnly),
                  self.used_jar,
                  self.process_dev_fix,
                  self.rank_model,
                  self.two_models,
                  self.update_model,
                  self.time_limit,
                  self.model_prepare,
                  self.simfix_mode,
                  self.patch_num
                  )

    def get_working_path(self):
        working_path = os.path.join(self.root, WORKING_REL_PATH)
        if not os.path.exists(working_path):
            os.mkdir(working_path)
        return working_path

    def get_simfix_working_path(self):
        working_path = os.path.join(self.root, 'simfix_exp_repo')
        return working_path

    def get_java8(self):
        return self.JDKDir

    def get_d4j_exe(self):
        while not os.path.isfile(self.d4j_exe):
            input_path = input('Please input the \"path2defects4j\":')
            self.d4j_exe = os.path.join(input_path, D4J_BIN)
            self.write_config_file()
        return self.d4j_exe

    def get_bears_exe(self):
        while not os.path.isdir(self.bears_exe):
            input_path = input('Please input the \"bears_exe\":')
            self.bears_exe = input_path
            self.write_config_file()
        return self.bears_exe

    def get_backup_path(self):
        backup_path = os.path.join(self.root, BACKUP_PATH)
        if not os.path.exists(backup_path):
            os.mkdir(backup_path)
        return backup_path

    def get_d4j_path(self):
        return os.path.join(self.root, LIB_FOLDER, D4J_FOLDER)

    def get_lib_path(self):
        return os.path.join(self.root, LIB_FOLDER)

    def get_junit_path(self):
        path = self.root
        for item in JUNIT_PATH:
            path = os.path.join(path, item)
        return path

    def get_hamcrest_path(self):
        path = self.root
        for item in HAMCREST_PATH:
            path = os.path.join(path, item)
        return path

    def get_jaid_path(self):
        return os.path.join(self.root, WORKING_REL_PATH, JAID_JAR)

    def get_jar_path(self):
        if self.used_jar == '' or self.used_jar == JAID_JAR:
            return self.get_jaid_path()
        else:
            return os.path.join(self.root, WORKING_REL_PATH, self.used_jar)

    def get_properties_folder_path(self):
        return os.path.join(self.root, PROPERTIES_FOLDER)

    def get_dev_fix_folder_path(self):
        return os.path.join(self.root, DEV_FIXED_FILES)

    def get_model_folder_path(self):
        return os.path.join(self.root, MODEL_FOLDER)

    def read_config_file(self, config_path):  # Reading configuration for this script
        self.thisPath = config_path
        if os.path.isfile(config_path):
            with open(config_path, 'r') as file:
                for line in file:
                    if line.startswith(JaidProperties.p_JDKDir):
                        self.JDKDir = line[line.index('=') + 1:].strip()
                    elif line.startswith(self.p_D4jExe):
                        self.d4j_exe = line[line.index('=') + 1:].strip()
                    elif line.startswith(self.p_BearsExe):
                        self.bears_exe = line[line.index('=') + 1:].strip()
                    elif line.startswith(JaidProperties.p_SbflAlgorithm):
                        self.SbflAlgorithm = line[line.index('=') + 1:].strip()
                    elif line.startswith(JaidProperties.p_RankingAlgorithm):
                        self.RankingAlgorithm = line[line.index('=') + 1:].strip()
                    elif line.startswith(JaidProperties.p_MaxTestNumber):
                        self.MaxTestNumber = line[line.index('=') + 1:].strip()
                    elif line.startswith(MyConfig.p_IcjBlackBoxOnly):
                        self.IcjBlackBoxOnly = line[line.index('=') + 1:].strip().lower() in ("yes", "true", "t", "1")
                    elif line.startswith(MyConfig.p_Jar):
                        self.used_jar = line[line.index('=') + 1:].strip()
                    elif line.startswith(MyConfig.p_ProcessDevFix):
                        self.process_dev_fix = line[line.index('=') + 1:].strip().lower() in ("yes", "true", "t", "1")
                    elif line.startswith(MyConfig.p_RankModel):
                        self.rank_model = line[line.index('=') + 1:].strip()
                    elif line.startswith(MyConfig.p_TwoModels):
                        self.two_models = line[line.index('=') + 1:].strip().lower() in ("yes", "true", "t", "1")
                    elif line.startswith(MyConfig.p_UpdateModel):
                        self.update_model = line[line.index('=') + 1:].strip().lower() in ("yes", "true", "t", "1")
                    elif line.startswith(MyConfig.p_TimeLimit):
                        self.time_limit = int(line[line.index('=') + 1:].strip())
                    elif line.startswith(MyConfig.p_ModelPrepare):
                        self.model_prepare = line[line.index('=') + 1:].strip().lower() in ("yes", "true", "t", "1")
                    elif line.startswith(MyConfig.p_SimfixMode):
                        self.simfix_mode = line[line.index('=') + 1:].strip()
                    elif line.startswith(MyConfig.p_SimfixPatchNum):
                        self.patch_num = int(line[line.index('=') + 1:].strip())
        else:
            self.JDKDir = get_java8_home()
            self.write_config_file()

    def write_config_file(self):
        with open(self.thisPath, 'w') as file:
            file.write(JaidProperties.p_JDKDir + '=' + self.JDKDir + os.linesep)
            file.write(MyConfig.p_D4jExe + '=' + self.d4j_exe + os.linesep)
            file.write(MyConfig.p_BearsExe + '=' + self.bears_exe + os.linesep)
            file.write(JaidProperties.p_SbflAlgorithm + '=' + self.SbflAlgorithm + os.linesep)
            file.write(JaidProperties.p_RankingAlgorithm + '=' + self.RankingAlgorithm + os.linesep)
            file.write(JaidProperties.p_MaxTestNumber + '=' + self.MaxTestNumber + os.linesep)
            file.write(MyConfig.p_IcjBlackBoxOnly + '=' + str(self.IcjBlackBoxOnly) + os.linesep)
            file.write(MyConfig.p_Jar + '=' + self.used_jar + os.linesep)
            file.write(MyConfig.p_ProcessDevFix + '=' + str(self.process_dev_fix) + os.linesep)
            file.write(MyConfig.p_RankModel + '=' + str(self.rank_model) + os.linesep)
            file.write(MyConfig.p_TwoModels + '=' + str(self.two_models) + os.linesep)
            file.write(MyConfig.p_UpdateModel + '=' + str(self.update_model) + os.linesep)
            file.write(MyConfig.p_TimeLimit + '=' + str(self.time_limit) + os.linesep)
            file.write(MyConfig.p_ModelPrepare + '=' + str(self.model_prepare) + os.linesep)
            file.write(MyConfig.p_SimfixMode + '=' + str(self.simfix_mode) + os.linesep)
            file.write(MyConfig.p_SimfixPatchNum + '=' + str(self.patch_num) + os.linesep)


def get_script_root_path():  # get current file name and working path
    py_path = os.path.realpath(__file__)
    script_path, script_name = os.path.split(py_path)
    root_path, src_folder = os.path.split(script_path)
    if src_folder == 'src':
        return root_path
    else:
        raise EnvironmentError('Cannot get root working path.')
