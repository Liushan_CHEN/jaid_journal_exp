from datetime import datetime

implements_to_string = lambda x: x
make_str = str
divide = '\t'


@implements_to_string
class TheResult(object):
    """A bugInfo object"""

    def __init__(self):
        super(TheResult, self).__init__()
        self.start_time = None
        self.monitor_finish_time = None
        self.build_snapshot_finish_time = None
        self.build_fix_finish_time = None
        self.all_finish_time = None
        self.first_plausible_time = None
        self.finish_validation_time = None
        self.finish_generation_time = None
        self.fix_line_size = 0
        self.etm_size = 0
        self.location_size = 0
        self.ranked_count = 0
        self.snapshot_size = 0
        self.snippet_size = 0
        self.fix_size = 0
        self.total_test = 0
        self.valid_test = 0
        self.passing_test = 0
        self.plausible_count = 0
        self.evaluated_count = 0

    def print_result(self):
        # print('total_test', 'valid_test', 'passing_test', sep=divide)
        # print(self.total_test, self.valid_test, self.passing_test, sep=divide)
        print('#HEADER#start_time', 'total_test', 'valid_test', 'passing_test',
              'location_size', 'etm_size', 'snapshot_size', 'snippet_size', 'fix_size', 'evaluated_count',
              'plausible_count', 'ranked_count', 'monitor_finish_time', 'first_plausible_time',
              'all_finish_time', 'build_snapshot_finish_time', 'generation_costing_time',
              'validation_costing_time', 'ranking_costing_time',
              sep=divide)

        output_strs = [str(self.start_time),
                       str(self.total_test), str(self.valid_test), str(self.passing_test),
                       str(self.location_size), str(self.etm_size), str(self.snapshot_size),
                       str(self.snippet_size),
                       str(self.fix_size), str(self.evaluated_count), str(self.plausible_count), str(self.ranked_count),
                       time_diff_str(self.start_time, self.monitor_finish_time),
                       time_diff_str(self.start_time, self.first_plausible_time),
                       time_diff_str(self.start_time, self.all_finish_time),
                       time_diff_str(self.start_time, self.build_snapshot_finish_time),
                       time_diff_str(self.build_snapshot_finish_time, self.finish_generation_time),
                       time_diff_str(self.finish_generation_time, self.finish_validation_time),
                       time_diff_str(self.finish_validation_time, self.all_finish_time)]
        output_str = divide.join(output_strs)
        print(output_str)
        result = [self.total_test, self.valid_test, self.passing_test]
        result.extend(output_strs)
        return result


class TheSnapshot(object):
    "SsId	dynamic	location_score	Bexp_similarity	passing	failing"

    def __init__(self, line_str):
        super(TheSnapshot, self).__init__()
        self.id = None
        self.idx = None
        self.location = None
        self.snapshotExpression = None
        self.value = None
        self.suspiciousness = None
        self.from_line(line_str)

    def from_line(self, line):
        line_con = split_log_line(line)
        line_obj = line_con[1].split(':: ')
        if len(line_obj) != 2:
            print('Wrong line format: ' + line)
        self.idx = int(line_obj[0])
        snapshot = get_obj_content(line_obj[1])
        self.id = snapshot['ID']
        self.location = snapshot['location']
        self.snapshotExpression = snapshot['snapshotExpression']
        self.value = get_boolean_value(snapshot['value'])
        self.suspiciousness = snapshot['suspiciousness']

    def opposite(self, other, diff):
        if self.location == other.location:
            if self.snapshotExpression == other.snapshotExpression:
                if not self.value == other.value:
                    if abs(self.idx - other.idx) < diff:
                        return True
        return False

    def __str__(self):
        return '%s{%s}' % (type(self).__name__, ', '.join('%s=%s' % item for item in vars(self).items()))


class TheFixAction(object):
    "costing_time   fixId 	strategy	Schema	snippet_similarity	fix_score ssid"

    def __init__(self, line_str, start_time):
        super(TheFixAction, self).__init__()
        self.fixid = None
        self.strategy = None
        self.schema = None
        self.snippet_similarity = None
        self.fix_score = 0.0
        self.location_score = 0.0
        self.state_score = 0.0
        self.ssid = None
        self.fix = ''
        self.location = 0
        line_con = split_log_line(line_str)
        if line_con:
            self.costing_time = parse_time(line_con[0])
            if start_time is not None:
                self.costing_time = time_diff_str(start_time, self.costing_time)
            self.from_line(line_con[1])
        else:
            self.from_line(line_str)

    def from_line(self, line):
        fix_action = get_obj_content(line)
        self.fixid = fix_action['fixId']
        self.snippet_similarity = fix_action['simi']
        self.fix_score = float(fix_action['desirability'])
        self.location_score = float(fix_action['locationScore'])
        self.state_score = float(fix_action['stateScore'])
        self.from_seed(fix_action['seed'])

    def from_seed(self, seed_str):
        seed_vals = seed_str.strip().split(';;')
        if len(seed_vals) != 3:
            print('Wrong seed_str format: ' + seed_str)
        self.strategy = get_seed_val(seed_vals[0])
        self.ssid = get_seed_val(seed_vals[1])
        self.schema = get_seed_val(seed_vals[2])

    def get_fix_str(self):
        fix_str = 'FixAction{fixId=' + self.fixid + ', seed=' + self.strategy + ';; ssID-' + self.ssid + ';; Schema-' + self.schema \
                  + ', simi=' + self.snippet_similarity + ', desirability=' + str(
            self.fix_score) + ', locationScore=' + str(self.location_score) + ', stateScore=' + str(
            self.state_score) + ', fix=[\n' \
                  + self.fix + ', location=' + str(self.location) + '\n'
        return fix_str

    def get_score_row(self):
        score_attr = [self.fixid, self.location_score, self.state_score, self.fix_score]
        # score_row = divide.join(score_attr)
        return score_attr


# ===========================
# Utils
# ===========================
def get_obj_content(obj_str):
    obj_dict = {}
    if '}' in obj_str:
        end_idx = obj_str.rindex('}')
    else:
        end_idx = len(obj_str)
    content = obj_str[obj_str.index('{') + 1:end_idx]
    contents = content.strip().split(', ')
    for item_str in contents:
        key, val = get_each_content_item(item_str)
        obj_dict[key] = val
    # print(obj_dict)
    return obj_dict


def get_each_content_item(pair_str):
    pair_str = pair_str.strip()
    if '=' in pair_str:
        idx = pair_str.index('=')
        if 0 <= idx <= len(pair_str):
            key = pair_str[0: idx]
            val = pair_str[idx + 1: len(pair_str)]
            return key, val
            # elif 0 <= idx:
            #     key = pair_str[0, idx - 1]
            #     return key, 'None'
    print('Wrong obj_item format: ' + pair_str)


def get_seed_val(seed_item):
    if '-' in seed_item:
        return seed_item[seed_item.index('-') + 1: len(seed_item)]
    else:
        return seed_item


def split_log_line(line):
    line_con = []
    info_str = '  [main ] INFO  - '
    debug_str = '  [main ] DEBUG - '
    if info_str in line:
        line_con = line.strip().split(info_str)
    elif debug_str in line:
        line_con = line.strip().split(debug_str)
    if len(line_con) == 2:
        return line_con
        # print('Wrong obj_item format: ' + line)


def parse_time(time_str):
    return datetime.strptime(time_str, "%Y-%m-%d %H:%M:%S,%f")


# def time_diff_str(start_t, end_t):
#     if start_t is not None and end_t is not None:
#         diff_t = end_t - start_t
#         time_str = str(diff_t)
#         if '.' in time_str:
#             return time_str[0:time_str.index('.')]
#         else:
#             print('time_diff_str with no \'.\' :' + time_str)
#             return time_str
#     return 'None'
def time_diff_str(start_t, end_t):
    if start_t is not None and end_t is not None:
        diff_t = end_t - start_t
        return str(diff_t.days * 60 * 24 + diff_t.seconds / 60)
    return 'None'


def time_diff_ms(start_t, end_t):
    if start_t is not None and end_t is not None:
        diff_t = end_t - start_t
        return str(diff_t.total_seconds())
    else:
        return 'None'


def get_boolean_value(result_str):
    boolean_debugger_result = get_obj_content(result_str)
    return boolean_debugger_result['value'].strip().lower() == 'true'
