import os
import subprocess
from distutils.dir_util import copy_tree

from benchmarks import AbsBenchMark
from config import D4J_BIN
from utils import split_src_num


class SimfixD4j(AbsBenchMark):

    def __init__(self, exp_config, bv):
        super().__init__(exp_config, bv)
        self.repo_name, self.bug_id = split_src_num(self.bv)
        self.repo_folder = get_repo_folder(self.repo_name.lower(), self.bug_id)
        self.repository_path = os.path.join(exp_config.get_simfix_working_path(), self.repo_name.lower(),
                                            self.repo_folder)
        self.__checkout_repo()

    def __run(self):
        pre_simfix_jar = 'PreSimFix-all.jar'
        os.chdir(self.exp_config.get_simfix_working_path())
        model_path, feature_path=self.get_model_features()

        if not os.path.isfile(pre_simfix_jar):
            cp_command = 'cp ' + os.path.join(self.exp_config.get_working_path(), pre_simfix_jar) + \
                         ' ' + os.path.join(self.exp_config.get_simfix_working_path(), pre_simfix_jar)
            os.system(cp_command)
            if not os.path.isfile(pre_simfix_jar):
                print(pre_simfix_jar + ' is not exitst!')

        if not os.path.isdir('sbfl'):
            unzip_conmmand='unzip -q sbfl.zip'
            os.system(unzip_conmmand)

        command = [repr(os.path.normpath(os.path.join(self.exp_config.JDKDir, 'bin', 'java'))).replace('\'', '')
            , '-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=22018', '-d64', '-Xms12g', '-Xmx14g',
                   '-jar', pre_simfix_jar,
                   '--proj_home=' + self.exp_config.get_simfix_working_path(),
                   '--proj_name=' + self.repo_name.lower(),
                   '--bug_id=' + self.bug_id,
                   '--d4j_home=' + self.exp_config.get_d4j_exe().replace(D4J_BIN, ''),
                   '--mode=' + self.exp_config.simfix_mode,
                   '--patch_num=' + str(self.exp_config.patch_num),
                   '--loc_num=200']
        if self.exp_config.update_model:
            command.append('--update_model')
        if self.exp_config.model_prepare:
            command.append('--model_prepare')
        else:
            command.append('--feature_path=' + feature_path)
            command.append('--model_path=' + model_path)


        comand_str = ' '.join(command)
        process = subprocess.Popen(command)
        try:
            print(str(process.pid) + ' EXE COMMAND: ' + comand_str)
            process.wait(self.exp_config.time_limit * 60)
        except subprocess.TimeoutExpired:
            print(str(self.exp_config.time_limit * 60) + ' min timed out - killing', process.pid)
            process.kill()
        print('FINISH EXECUTING COMMAND: ' + comand_str)

    def __checkout_repo(self, is_buggy=True):
        if not os.path.exists(self.repository_path):
            repo_path = os.path.join('/tmp', self.repo_folder)
            if is_buggy:
                version = 'b'
            else:
                version = 'f'
            command = self.exp_config.get_d4j_exe() + ' checkout -p ' + self.repo_name + ' -v ' + self.bug_id \
                      + version + ' -w ' + repo_path
            print(command)
            os.system(command)
            if os.path.exists(repo_path):
                prevdir = os.getcwd()
                os.chdir(repo_path)
                os.system(self.exp_config.get_d4j_exe() + ' compile')
                os.chdir(prevdir)
                copy_tree(repo_path, self.repository_path)

    def prepare_bv(self):
        self.__checkout_repo()

    def run_bv(self):
        self.__checkout_repo()
        self.__run()

    def read_bv(self):
        pass

    def get_model_features(self):
        default_model = 'default'
        if len(self.exp_config.rank_model):
            model_path = os.path.join(
                self.exp_config.get_model_folder_path(),
                self.exp_config.rank_model + '-' + self.repo_name.lower() + '_' + str(self.bug_id) + '.bin')
            feature_path = os.path.join(
                self.exp_config.get_model_folder_path(),
                self.exp_config.rank_model + '-' + self.repo_name.lower() + '_' + str(self.bug_id) + '-feature.txt')
            if not os.path.isfile(model_path) or not os.path.isfile(feature_path):
                model_path = os.path.join(
                    self.exp_config.get_model_folder_path(),
                    self.exp_config.rank_model + '-' + self.repo_name + str(self.bug_id) + '.bin')
                feature_path = os.path.join(
                    self.exp_config.get_model_folder_path(),
                    self.exp_config.rank_model + '-' + self.repo_name + str(self.bug_id) + '-feature.txt')
            if not os.path.isfile(model_path) or not os.path.isfile(feature_path):
                model_path = os.path.join(
                    self.exp_config.get_model_folder_path(),
                    self.exp_config.rank_model + '-' + default_model + '.bin')
                feature_path = os.path.join(
                    self.exp_config.get_model_folder_path(),
                    self.exp_config.rank_model + '-' + default_model + '-feature.txt')
            return model_path, feature_path

def get_repo_folder(bug, id):
    return bug.lower() + '_' + str(id) + '_buggy'

