import os
import shutil
from shutil import copyfile

import constants
from download_google_file import download_file_from_google_drive
from utils import get_max_backup_folder, is_desired_size, is_desired_name, \
    is_modified_file


# =====================================================================================================
# Operation Function Definitions
# =====================================================================================================
def show_config(exp_config):
    print("Config: ", str(exp_config))


def download_jaid(exp_config):
    download_file_from_google_drive(constants.JAID_JAR, exp_config.get_jaid_path())


def update_exp_control(exp_config, new_sbfl=None, new_ranking=None, new_max_test=None, icj_black=None, d4j_path=None,
                       used_jar=None, process_dev_fix=None, rank_model=None, two_models=None, update_model=None,
                       time_limit=None,model_prepare=None,simfix_mode=None,patch_num=None):
    if new_sbfl:
        exp_config.SbflAlgorithm = new_sbfl
    if new_ranking:
        exp_config.RankingAlgorithm = new_ranking
    if new_max_test:
        exp_config.MaxTestNumber = new_max_test
    if icj_black:
        exp_config.IcjBlackBoxOnly = icj_black.lower() in ("yes", "true", "t", "1")
    if d4j_path:
        exp_config.d4j_exe = d4j_path
    if used_jar:
        exp_config.used_jar = used_jar
    if process_dev_fix:
        exp_config.process_dev_fix = process_dev_fix
    if rank_model:
        exp_config.rank_model = rank_model
    if two_models:
        exp_config.two_models = two_models
    if update_model:
        exp_config.update_model = update_model
    if time_limit:
        exp_config.time_limit = time_limit
    if model_prepare:
        exp_config.model_prepare = model_prepare
    if simfix_mode:
        exp_config.simfix_mode = simfix_mode
    if patch_num:
        exp_config.patch_num = patch_num
    exp_config.write_config_file()


def rm_last_max_backup_folder(backup_dir):
    o_backup_id = get_max_backup_folder(backup_dir)
    if o_backup_id is not None:
        o_backup_dir = os.path.join(backup_dir, constants.BACKUP_FOLDER_NAME + str(o_backup_id))
        shutil.rmtree(o_backup_dir)
        print(o_backup_dir + ' is removed')


def tracking_all_results(working_dir, backup_dir):  # Find all experiment results in the working dir and backup them
    o_backup_id = get_max_backup_folder(backup_dir)
    o_backup_dir = None
    if o_backup_id is not None:
        o_backup_dir = os.path.join(backup_dir, constants.BACKUP_FOLDER_NAME + str(o_backup_id))
        n_backup_dir = os.path.join(backup_dir, constants.BACKUP_FOLDER_NAME + str(int(o_backup_id) + 1))
    else:
        n_backup_dir = os.path.join(backup_dir, constants.BACKUP_FOLDER_NAME + '0')
    os.mkdir(n_backup_dir)
    print('Backup stored in folder: ' + n_backup_dir)

    for x_repo in os.listdir(working_dir):
        x_repo_dir = os.path.join(working_dir, x_repo)
        if os.path.isdir(x_repo_dir):
            print('Checking ' + x_repo + ' ...')
            if x_repo in constants.D4J_CANDIDATES.keys():  # backup output for d4j bugs
                back_up_output(x_repo_dir, x_repo, o_backup_dir, n_backup_dir)
            elif x_repo in constants.ICJ_CANDIDATES.keys():
                pass
            else:
                print('folder ' + x_repo + ' is not a desired repository.')
    print('tracking_all_results is done')


def back_up_output(repository_path, repo_folder, o_backup_dir, n_backup_dir):
    for folder in os.listdir(repository_path):
        if folder == constants.OUTPUT_FOLDER_NAME:  # find the experiment output in repo
            jaid_output_dir = os.path.join(repository_path, folder)
            print('Copying ' + repository_path + ' ...')

            for exp_output in os.listdir(jaid_output_dir):
                src = os.path.join(jaid_output_dir, exp_output)
                dst = os.path.join(n_backup_dir, repo_folder)

                if is_desired_size(src) and is_desired_name(src):
                    if not os.path.isdir(dst):
                        os.mkdir(dst)
                    if o_backup_dir is None or is_modified_file(os.path.join(o_backup_dir, repo_folder),
                                                                jaid_output_dir, exp_output):
                        copyfile(src, os.path.join(dst, exp_output))
                        # else:
                        #     os.link(os.path.join(os.path.join(o_backup_dir, repo_folder), exp_output),
                        #             os.path.join(dst, exp_output))
            break


def clear_output(exp_config):
    for x_repo in os.listdir(exp_config.get_working_path()):
        x_repo_dir = os.path.join(exp_config.get_working_path(), x_repo)
        if os.path.isdir(x_repo_dir):
            for dirname, dirnames, filenames in os.walk(x_repo_dir):
                dump_file = os.path.join(dirname, constants.DUMP_FILE)
                hs_err_file = os.path.join(dirname, 'hs_err_.log')
                if os.path.isfile(dump_file):
                    os.remove(dump_file)
                if os.path.isfile(hs_err_file):
                    os.remove(hs_err_file)
                for subdirname in dirnames:
                    if subdirname == constants.OUTPUT_FOLDER_NAME:  # find the experiment output in repo
                        jaid_output_dir = os.path.join(dirname, subdirname)
                        shutil.rmtree(jaid_output_dir)
            if x_repo.startswith('Bears-'):
                shutil.rmtree(x_repo_dir)
    for folder in os.listdir(exp_config.get_simfix_working_path()):
        if folder in ('log','patch','score','PreSimFix-all.jar'):
            folder_dir = os.path.join(exp_config.get_simfix_working_path(), folder)
            if os.path.isdir(folder_dir):
                shutil.rmtree(folder_dir)
            if os.path.isfile(folder_dir):
                os.remove(folder_dir)


def print_available_bv():
    # print('Available D4J repositories name and corresponding identifier:')
    # print(constants.repositories)
    print('All available project and buggy ids:')
    print('<REPO_NAME>: <BUG_VERSION_1>,<BUG_VERSION2>,...')
    for project in constants.valid_bvs.keys():
        print(project + ' : ' + ','.join(sorted(set(constants.valid_bvs[project]))))
        print('\n')
    print('=' * 10)
    print('Defects4J <BUG_ID> format:' + constants.D4J_BUGID_FORMAT + ' (e.g.,Lang33)')
    print('IntroClassJava <BUG_ID> format:' + constants.ICJ_BUGID_FORMAT + ' (e.g.,checksum-e23b9_005)')
    print('QuixBugs <BUG_ID> format:' + constants.QB_BUGID_FORMAT + ' (e.g.,QB_SHORTEST_PATHS)')
    print('=' * 10)
    print('Example for running JAID to fix a buggy program:')
    print(
        '$ python3 <PATH_TO_jaid_exp_pre>/src/main.py ' + constants.COMMAND_RUN + ' Lang33,checksum-e23b9_005,QB_SHORTEST_PATHS')
    # print('All available IntroClassJava repositories:')
    # print(','.join(constants.ICJ_CANDIDATES.keys()))
    # print('Example for reading JAID\'s output of fixing a buggy program:')
    # print('$ python3 <PATH_TO_jaid_exp_pre>/src/main.py ' + constants.COMMAND_READ + ' Lang33,checksum-e23b9_005')
