import filecmp
import os
import re
import urllib.request
import zipfile
from pathlib import Path

import pip
from pip.operations import freeze

from constants import valid_bvs, BACKUP_FOLDER_NAME


def unzip(source_zip, target_dir):
    myzip = zipfile.ZipFile(source_zip)
    myzip.extractall(path=target_dir)
    myzip.close()


def zip_file(folder2zip, zip_file_path):
    f = zipfile.ZipFile(zip_file_path, 'w', zipfile.ZIP_DEFLATED)
    current_dir = os.getcwd()
    os.chdir(folder2zip)

    for dir_path, dir_names, file_names in os.walk(folder2zip):
        for filename in file_names:
            f.write(os.path.join(dir_names, filename))
            # f.write(filename)
    f.close()
    os.chdir(current_dir)


def download_project(url, file_path):
    urllib.request.urlretrieve(url, file_path)


def get_java8_home():
    java_home = os.getenv("JAVA_HOME")
    while True:
        if is_correct_jdk_home(java_home):
            print('JDK path for JAID experiment is saved in the \'setting.properties\' file under script\'s directory.')
            return java_home
        java_home = input(
            'Fail to locate JDK8 automatically, please input a correct JDK8 path:\n '
            '(e.g., For Mac OS: \"/Library/Java/JavaVirtualMachines/jdk1.8.0_31.jdk/Contents/Home\";\n'
            'For Windows: \"C:\Program Files\Java\jdk1.8.0_131\";\n'
            'For Linux: \"/usr/lib/jvm/java-8-openjdk-amd64\")\n')


def is_correct_jdk_home(java_home):
    if not java_home:
        print('')
    elif 'jdk' not in java_home.lower():
        print('\nThe input path is not a JDK path.')
    elif not os.path.exists(java_home):
        print('\nThe input path is not exist.')
    else:
        if '8' not in java_home:
            print('\nThere is no guarantee that JAID works in other JDK version.')
        return True
    return False


def init_env(requirement_path):
    try:
        if not check_requirement(requirement_path):
            pip.main(["install", "-r", requirement_path])
        return True
    except Exception as detail:
        print(detail)
        print("Unable to install " + requirement_path +
              " using pip. Please read the instructions for manual installation.. Exiting")
        return False


def check_requirement(requirement_path):
    r_file = []
    r_current = []
    with open(requirement_path, 'r') as file:
        for line in file:
            r_file.append(line)
    x = freeze.freeze()
    for p in x:
        r_current.append(p)
    # print('r_current:' + str(r_current))
    # print('r_file:' + str(r_file))
    count = 0
    for one_r in r_file:
        if one_r.strip() in r_current:
            count += 1
    if count == len(r_file):
        return True
    return False


def get_icj_bug_id(repo_name, author_id, version_id):
    return repo_name + '_' + author_id[0:5] + '_' + version_id


def split_icj_bug_id(bug_id):
    strs = bug_id.split('_')
    if len(strs) == 2:
        author_id = strs[0]
        bv = strs[1]
    elif len(strs) == 3:
        author_id = strs[1]
        bv = strs[2]
    else:
        print('ICJ bugid error.')
        return
    return author_id, bv


def split_src_num(candidate):
    r = re.compile("([a-zA-Z]+)([0-9]+)")
    m = r.match(candidate)
    repo_name = m.group(1)
    bug_id = m.group(2)
    return repo_name, bug_id


def split_icj_repo_bugid(candidate):
    items = candidate.split('-')
    if len(items) == 2 and items[0] in valid_bvs.keys() and items[1] in valid_bvs[items[0]]:
        return items[0], items[1]
    else:
        print('Input repository name or bug id error.')


def get_max_backup_folder(dst):
    backup_folders = {}
    for f_x in os.listdir(dst):
        if f_x.startswith(BACKUP_FOLDER_NAME):
            backup_id = f_x[7:len(f_x)]
            backup_folders[backup_id] = f_x
    if len(backup_folders):
        return max(backup_folders.keys(), key=int)


def is_modified_file(o_backup_path, jaid_output_path, file_name):
    jaid_f = os.path.join(jaid_output_path, file_name)
    backup_f = os.path.join(o_backup_path, file_name)
    if os.path.isfile(backup_f):
        is_equal = filecmp.cmp(jaid_f, backup_f, shallow=False)
        return not is_equal
    else:
        return True


def is_desired_name(path):
    if os.path.isfile(path):
        return True
    return False


def is_desired_size(path):
    if os.path.getsize(path) < 10 ** 9:
        return 1
    return 0


def get_all_files_recursively(path, file_type):
    file_map = {}
    if os.path.exists(path):
        for file in Path(path).glob(file_type):
            file_map[file.name] = os.path.join(path, file)
    return file_map
