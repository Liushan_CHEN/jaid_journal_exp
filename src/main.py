#! /usr/bin/env python3
import argparse
import os

import constants
from benchmarks import IcjBenchmark, D4jBenchMark, QbBenchMark, BearsBenchMark
from config import get_script_root_path, MyConfig
from simfix_exp import SimfixD4j
from utils import init_env

init_env(os.path.join(get_script_root_path(), constants.REQUIREMENT_FILE_NAME))

from fuctions import print_available_bv, show_config, tracking_all_results, rm_last_max_backup_folder, \
    update_exp_control, clear_output


def add_command(com_group, command_key):
    com_group.add_argument(command_key, action='store_true',
                           help=constants.Commands[command_key], required=False)


parser = argparse.ArgumentParser(description='Script for JAID experiment buggy repositories.\n')

exp_com = parser.add_argument_group('Experiment actions')
add_command(exp_com, constants.COMMAND_SHOW_INFO)
add_command(exp_com, constants.COMMAND_SHOW_CONFIG)
add_command(exp_com, constants.COMMAND_BACKUP)
add_command(exp_com, constants.COMMAND_RM_BACKUP)
add_command(exp_com, constants.COMMAND_CLEAR)

exp_com.add_argument(constants.COMMAND_SET_SBFL,
                     metavar='[AutoFix / Tarantula / Barinel / DStar / Ochiai / Op2 / all]',
                     help=constants.Commands[constants.COMMAND_SET_SBFL], required=False)
exp_com.add_argument(constants.COMMAND_SET_RANKING,
                     metavar='[AutoFix / Qlose / all / NoRank]',
                     help=constants.Commands[constants.COMMAND_SET_RANKING], required=False)
exp_com.add_argument(constants.COMMAND_SET_ICJ_BLACK_TEST,
                     metavar='[T / F]',
                     help=constants.Commands[constants.COMMAND_SET_ICJ_BLACK_TEST], required=False)
exp_com.add_argument(constants.COMMAND_SET_MAX_TEST,
                     metavar='Specific number',
                     help=constants.Commands[constants.COMMAND_SET_MAX_TEST], required=False)
exp_com.add_argument(constants.COMMAND_SET_D4J,
                     metavar='[Path/to/Defects4J]',
                     help=constants.Commands[constants.COMMAND_SET_D4J], required=False)
exp_com.add_argument(constants.COMMAND_SET_USED_JAR,
                     metavar='Name of the jar file for experiment',
                     help=constants.Commands[constants.COMMAND_SET_USED_JAR], required=False)
exp_com.add_argument(constants.COMMAND_SET_PROCESS_DEV_FIX,
                     metavar='[T / F]',
                     help=constants.Commands[constants.COMMAND_SET_PROCESS_DEV_FIX], required=False)
exp_com.add_argument(constants.COMMAND_SET_RANK_MODEL,
                     metavar='noKill/full',
                     help=constants.Commands[constants.COMMAND_SET_RANK_MODEL], required=False)
exp_com.add_argument(constants.COMMAND_SET_TWO_MODELS,
                     metavar='T/F',
                     help=constants.Commands[constants.COMMAND_SET_TWO_MODELS], required=False)
exp_com.add_argument(constants.COMMAND_UPDATE_MODEL,
                     metavar='T/F',
                     help=constants.Commands[constants.COMMAND_UPDATE_MODEL], required=False)
exp_com.add_argument(constants.COMMAND_TIME_LIMIT,
                     metavar='minutes',
                     help=constants.Commands[constants.COMMAND_TIME_LIMIT], required=False)
exp_com.add_argument(constants.COMMAND_MODEL_PREPARE,
                     metavar='T/F',
                     help=constants.Commands[constants.COMMAND_MODEL_PREPARE], required=False)
exp_com.add_argument(constants.COMMAND_SIMFIX_MODE,
                     metavar='pre/restore/simfix/pre1',
                     help=constants.Commands[constants.COMMAND_SIMFIX_MODE], required=False)
exp_com.add_argument(constants.COMMAND_PATCH_NUM,
                     metavar='1',
                     help=constants.Commands[constants.COMMAND_PATCH_NUM], required=False)

exp_com.add_argument(constants.COMMAND_PREPARE,
                     metavar=constants.ARGS_FORMAT,
                     help=constants.Commands[constants.COMMAND_PREPARE], required=False)
exp_com.add_argument(constants.COMMAND_READ,
                     metavar=constants.ARGS_FORMAT,
                     help=constants.Commands[constants.COMMAND_READ], required=False)
exp_com.add_argument(constants.COMMAND_RUN,
                     metavar=constants.ARGS_FORMAT,
                     help=constants.Commands[constants.COMMAND_RUN], required=False)
exp_com.add_argument(constants.COMMAND_SIMFIX,
                     metavar=constants.ARGS_FORMAT,
                     help=constants.Commands[constants.COMMAND_SIMFIX], required=False)

args = parser.parse_args()

# =====================================================================================================
# Processing operations
# =====================================================================================================

exp_config = MyConfig()
exp_config.read_config_file(os.path.join(get_script_root_path(), constants.PROPERTIES_FILE_NAME))
# exp_config = MyConfig()
init_env(os.path.join(get_script_root_path(), constants.REQUIREMENT_FILE_NAME))

if args.info:
    print_available_bv()
elif args.exp_config:
    show_config(exp_config)


elif args.sbfl:
    update_exp_control(exp_config, args.sbfl)
elif args.ranking:
    update_exp_control(exp_config, new_ranking=args.ranking)
elif args.max_test:
    update_exp_control(exp_config, new_max_test=args.max_test)
elif args.icj_black:
    update_exp_control(exp_config, icj_black=args.icj_black)
elif args.d4j_path:
    update_exp_control(exp_config, d4j_path=args.d4j_path)
elif args.used_jar:
    update_exp_control(exp_config, used_jar=args.used_jar)
elif args.process_dev_fix:
    update_exp_control(exp_config, process_dev_fix=args.process_dev_fix)
elif args.rank_model:
    update_exp_control(exp_config, rank_model=args.rank_model)
elif args.two_models:
    update_exp_control(exp_config, two_models=args.two_models)
elif args.update_model:
    update_exp_control(exp_config, update_model=args.update_model)
elif args.time_limit:
    update_exp_control(exp_config, time_limit=args.time_limit)
elif args.model_prepare:
    update_exp_control(exp_config, model_prepare=args.model_prepare)
elif args.simfix_mode:
    update_exp_control(exp_config, simfix_mode=args.simfix_mode)
elif args.patch_num:
    update_exp_control(exp_config, patch_num=args.patch_num)


# ============== Available actions for experiment
elif args.read:
    csv_path = os.path.join(exp_config.get_working_path(), constants.ICJ_RESULT_FILE_NAME)
    if os.path.isfile(csv_path):
        os.remove(csv_path)
    bvs = args.read.split(',')
    for bv in bvs:
        if '-' in bv:  # icj repo bug id
            icj_bm = IcjBenchmark(exp_config, bv)
            icj_bm.read_bv()
        else:  # d4j repo bug id
            d4j_bm = D4jBenchMark(exp_config, bv)
            d4j_bm.read_bv()

elif args.run:
    bvs = args.run.split(',')
    for bv in bvs:
        if 'Bears-' in bv:  # Bears repo bug id
            bears_bm = BearsBenchMark(exp_config, bv)
            bears_bm.run_bv()
        elif '-' in bv:  # icj repo bug id
            icj_bm = IcjBenchmark(exp_config, bv)
            icj_bm.run_bv()
        elif bv.startswith(constants.QB_PREFIX):
            qb_bm = QbBenchMark(exp_config, bv)
            qb_bm.run_bv()
        else:  # d4j repo bug id
            d4j_bm = D4jBenchMark(exp_config, bv)
            d4j_bm.run_bv()

elif args.simfix:
    bvs = args.simfix.split(',')
    for bv in bvs:
        d4j_bm = SimfixD4j(exp_config, bv)
        d4j_bm.run_bv()

elif args.pre:
    bvs = args.pre.split(',')
    for bv in bvs:
        if 'Bears-' in bv:  # Bears repo bug id
            bears_bm = BearsBenchMark(exp_config, bv)
            bears_bm.prepare_bv()
        elif '-' in bv:  # icj repo bug id
            icj_bm = IcjBenchmark(exp_config, bv)
            icj_bm.prepare_bv()
        else:  # d4j repo bug id
            d4j_bm = D4jBenchMark(exp_config, bv)
            d4j_bm.prepare_bv()
elif args.backup:
    tracking_all_results(exp_config.get_working_path(), exp_config.get_backup_path())
elif args.rm_backup:
    rm_last_max_backup_folder(exp_config.get_backup_path())
elif args.clear:
    clear_output(exp_config)
