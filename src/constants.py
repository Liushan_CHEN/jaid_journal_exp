PROPERTIES_FILE_NAME = 'setting.properties'
REQUIREMENT_FILE_NAME = 'requirements.txt'
ICJ_RESULT_FILE_NAME = 'icj_result.csv'
FIX_RANKING_SCORE_FILE_NAME = 'fix_ranking_score.csv'
ANALYSIS_TMP = 'analysis_tmp.txt'
PLAUSIBLE_FIX_ACTIONS_LOG = 'plausible_fix_actions.log'
MAIN_LOG = 'jaid.log'
ZIP_SUFFIX = '.zip'
JAID_ARG_PROPERTY = '--JaidSettingFile'
# JAID_ARG_PROPERTY = '--FixjaSettingFile'
OUTPUT_FOLDER_NAME = 'jaid_output'
ARGS_FORMAT = '<BUG_ID1,BUG_ID2,...>'
D4J_BUGID_FORMAT = '<REPO_NAME><DEFECTS4J_VERSION_ID>'
ICJ_BUGID_FORMAT = '<REPO_NAME>-<AUTHOR_ID>_<VERSION_ID>'
QB_PREFIX = 'QB_'
QB_BUGID_FORMAT = QB_PREFIX + '<CLASS_NAME>'
BACKUP_FOLDER_NAME = 'backup_'
JAVA_SUFFIX = ".java"
DUMP_FILE = 'dump_Oome.hprof'

repositories = {'closure': 'closure-compiler',
                'lang': 'commons-lang',
                'math': 'commons-math',
                'time': 'joda-time',
                'chart': 'JFreechart',
                'mockito': 'mockito'}

valid_bvs = {
    'closure': ['1', '10', '104', '113', '118', '125', '126', '18', '31', '33', '40', '5', '62', '63', '70', '73',
                '86', '10', '125', '126', '14', '33', '40', '51', '62', '63', '70', '73'],
    'lang': ['24', '33', '38', '39', '45', '51', '55', '61', '10', '24', '33', '35', '38', '39', '43', '44', '45', '46',
             '51', '53', '55', '57', '58', '59', '6', '7'],
    'math': ['101', '105', '32', '5', '50', '53', '70', '80', '82', '85', '105', '2', '25', '28', '3', '32', '33', '34',
             '35', '40', '42', '49', '5', '50', '53', '57', '58', '69', '70', '73', '78', '8', '80', '81', '82', '84',
             '85', '87', '88', '89', '90', '93', '95', '97', '99'],
    'time': ['19', '15', '4'],
    'chart': ['1', '13', '14', '15', '19', '21', '24', '25', '26', '3', '5', '7', '8', '9'],
    'mockito': ['38', '24', '1', '7', '29', '28', '33', '22', '11', '12', '18', '3', '21', '5', '34', '6', '4', '37',
                '8', '13', '27', '20', '35'],
    'checksum': ['659a7_003', '2c155_003', '36d80_003', 'a0e3f_005', 'a0e3f_000', 'e23b9_005', 'e9c74_000', '08c7e_010',
                 '08c7e_007', '08c7e_011', '08c7e_006'],
    'digits': ['d6364_000', '48b82_000', '313d5_000', '6e464_003', '6e464_004', '6e464_000', 'd8b26_000', '295af_003',
               '295af_000', '295af_002', 'f2997_000', 'f2997_002', 'c9d71_001', 'c9d71_000', '98d87_003', '98d87_001',
               '98d87_004', '98d87_000', 'ca505_003', 'f227e_000', 'ca94e_000', 'ca94e_002', 'a0e3f_003', '1b31f_000',
               '1b31f_002', '0cea4_000', '0cea4_002', '317aa_004', '317aa_002', 'cd2d9_003', '5b504_000', 'c5d8f_003',
               'e9c74_000', 'd43d3_000', '833bd_000', '387be_000', '387be_002', 'd1204_001', 'd1204_000', '1c2bb_003',
               '1c2bb_000', 'd767a_007', '65e02_015', '65e02_004', 'bfad6_003', 'bfad6_005', 'bfad6_004', 'bfad6_000',
               '08c7e_001', '08c7e_000', '3214e_003', '3214e_000', '90a14_004', '90a14_000', '1391c_000', '1391c_002',
               'd2b88_003', 'd2b88_001', 'd2b88_005', 'd2b88_004', 'd2b88_006', 'd2b88_000', '0cdfa_007', '0cdfa_005',
               '0cdfa_004', '0cdfa_006', '88394_003', '88394_004', 'e79f8_000', 'e79f8_002', '07045_000', '07045_002',
               'd5059_000', '2af3c_000', '9013b_001'],
    'grade': ['b1924_003', 'b1924_001', 'b1924_000', 'd6364_007', '48b82_000', 'dccb1_001', '6e464_000', 'd8b26_000',
              '769cd_000', '295af_010', 'aacea_003', 'af81f_009', 'af81f_001', 'af81f_007', 'af81f_000', 'af81f_002',
              'f5b56_010', 'f5b56_000', 'ee1f2_000', 'c9d71_003', 'c9d71_001', 'c9d71_004', 'c9d71_000', 'c9d71_002',
              '98d87_004', '68ea5_000', '36d80_000', 'ca94e_000', 'a0e3f_010', 'a0e3f_012', '1b31f_003', '3cf6d_010',
              '0cea4_003', '0cea4_001', '0cea4_000', '0cea4_002', 'e23b9_003', 'e23b9_001', 'e23b9_004', 'e23b9_000',
              'e23b9_002', '317aa_003', '317aa_001', '317aa_004', '317aa_000', 'cd2d9_008', 'cd2d9_009', 'cd2d9_003',
              'cd2d9_010', 'cd2d9_007', 'cd2d9_013', 'cd2d9_006', '5b504_000', '48925_010', '48925_000', '9c930_003',
              '9c930_007', '9c930_000', '833bd_003', '387be_000', '92b7d_002', '1c2bb_003', '1c2bb_000', '95362_014',
              '95362_015', '95362_010', '95362_013', '95362_018', 'fe9d5_003', 'fe9d5_004', 'bfad6_001', 'bfad6_000',
              '93f87_015', '30074_004', 'dc11f_000', '89b1a_003', '0cdfa_003', '90834_009', '90834_010', '90834_013',
              '3b237_007', 'd009a_003', 'd009a_000', 'cb243_001', 'cb243_000', 'b6fd4_000', '07045_002', '9013b_000',
              '75c98_003'],
    'median': ['48b82_000', '6e464_003', '2c155_000', 'aacea_003', 'af81f_007', 'af81f_004', '36d80_000', '6aaea_000',
               '1b31f_000', '3cf6d_007', '0cea4_003', '1bf73_003', '1bf73_000', '317aa_003', '317aa_000', '317aa_002',
               'cd2d9_010', '15cb0_003', '9c930_003', '9c930_007', '9c930_012', 'e9c62_001', 'e9c62_000', 'd43d3_000',
               'd1204_000', '1c2bb_000', '95362_003', '95362_000', 'fe9d5_000', 'fe9d5_002', '93f87_015', '93f87_010',
               '93f87_012', 'c716e_001', 'c716e_000', 'c716e_002', '30074_000', '89b1a_003', '89b1a_010', '89b1a_007',
               '90a14_000', 'd2b88_000', '0cdfa_003', 'd4aae_000', '68eb0_000', '90834_003', '90834_015', '90834_010',
               '3b237_003', '3b237_006', 'd009a_000', 'fcf70_003', 'fcf70_000', 'fcf70_002', 'b6fd4_001', 'b6fd4_000',
               '9013b_000'],
    'smallest': ['48b82_001', '48b82_000', '769cd_009', '769cd_003', '769cd_010', '769cd_007', '769cd_004', '769cd_002',
                 'af81f_000', 'f2997_000', 'c9d71_003', 'c9d71_000', '97f6b_003', '36d80_003', '6aaea_001', '6aaea_000',
                 '1b31f_003', '3cf6d_003', 'f8d57_000', '15cb0_007', 'e9c62_000', '2694a_000', '95362_009', '818f8_003',
                 '818f8_002', '93f87_000', 'c868b_000', '30074_007', '30074_000', 'ea67b_003', '90a14_001', '90a14_000',
                 '5a568_000', '68eb0_000', '88394_003', '88394_007', '88394_006', '88394_002', '90834_005', '3b237_008',
                 '3b237_003', '3b237_007', '3b237_006', '84602_007', 'd009a_001', 'cb243_000', 'dedc2_000', '346b1_003',
                 '346b1_010', '346b1_005', '346b1_002', '9013b_000'],
    'syllables': ['99cbb_003', 'ca505_003', '36d80_003', 'f8d57_002', '48925_007', '818f8_007', '38eb9_003',
                  '38eb9_004', '90a14_000', 'fcf70_002', 'b6fd4_000', 'd5059_000', '2af3c_003'],
    'QB': ['REVERSE_LINKED_LIST', 'TOPOLOGICAL_ORDERING', 'POSSIBLE_CHANGE', 'DEPTH_FIRST_SEARCH', 'KNAPSACK', 'PASCAL',
           'MAX_SUBLIST_SUM', 'HANOI', 'NEXT_PALINDROME', 'FIND_IN_SORTED', 'QUICKSORT', 'IS_VALID_PARENTHESIZATION',
           'SIEVE', 'SQRT', 'LIS', 'FIND_FIRST_IN_SORTED', 'RPN_EVAL', 'GCD', 'DETECT_CYCLE', 'KTH',
           'MINIMUM_SPANNING_TREE', 'FLATTEN', 'NEXT_PERMUTATION', 'BITCOUNT', 'TO_BASE', 'SUBSEQUENCES', 'LCS_LENGTH',
           'BREADTH_FIRST_SEARCH', 'LONGEST_COMMON_SUBSEQUENCE', 'MERGESORT', 'BUCKETSORT', 'SHUNTING_YARD',
           'SHORTEST_PATHS', 'LEVENSHTEIN', 'GET_FACTORS', 'WRAP', 'SHORTEST_PATH_LENGTH', 'SHORTEST_PATH_LENGTHS',
           'KHEAPSORT', 'POWERSET']}

COMMAND_SHOW_INFO = '--info'
COMMAND_SHOW_INFO_DESC = 'Display available projects and bug ids.'

COMMAND_SHOW_CONFIG = '--exp_config'
COMMAND_SHOW_CONFIG_DESC = 'Display the configuration file dir for experiment and its contents.'

COMMAND_SET_SBFL = '--sbfl'
COMMAND_SET_SBFL_DESC = 'Set SBFL algorithm'

COMMAND_SET_USED_JAR = '--used_jar'
COMMAND_SET_USED_JAR_DESC = 'Set used jar for experiment'

COMMAND_SET_PROCESS_DEV_FIX = '--process_dev_fix'
COMMAND_SET_PROCESS_DEV_FIX_DESC = 'Set if process_dev_fix for experiment'

COMMAND_SET_RANK_MODEL = '--rank_model'
COMMAND_SET_RANK_MODEL_DESC = 'Set if using rank_model for ranking candidates'

COMMAND_SET_TWO_MODELS = '--two_models'
COMMAND_SET_TWO_MODELS_DESC = 'Set if using two models for ranking candidates'

COMMAND_UPDATE_MODEL = '--update_model'
COMMAND_UPDATE_MODEL_DESC = 'Set if update models after each loop'

COMMAND_MODEL_PREPARE = '--model_prepare'
COMMAND_MODEL_PREPARE_DESC = 'Set if is preparing model'

COMMAND_SIMFIX_MODE = '--simfix_mode'
COMMAND_SIMFIX_MODE_DESC = 'Set simfix_mode'

COMMAND_PATCH_NUM = '--patch_num'
COMMAND_PATCH_NUM_DESC = 'Set simfix patch_num'

COMMAND_TIME_LIMIT = '--time_limit'
COMMAND_TIME_LIMIT_DESC = 'Set time_limit in minutes'

COMMAND_SET_RANKING = '--ranking'
COMMAND_SET_RANKING_DESC = 'Set ranking algorithm'

COMMAND_SET_D4J = '--d4j_path'
COMMAND_SET_D4J_DESC = 'Set executable Defect4J path'

COMMAND_SET_MAX_TEST = '--max_test'
COMMAND_SET_MAX_TEST_DESC = 'Set maximum number of involving passing test (-1 as no limitation)'

COMMAND_SET_ICJ_BLACK_TEST = '--icj_black'
COMMAND_SET_ICJ_BLACK_TEST_DESC = '(T/F) Whether using black-box test only on ICJ'

COMMAND_PREPARE = '--pre'
COMMAND_PREPARE_DESC = 'Prepare specific buggy version (property file)'

COMMAND_RUN = '--run'
COMMAND_RUN_DESC = 'Execute JAID against specific buggy program(s)'

COMMAND_READ = '--read'
COMMAND_READ_DESC = 'Read JAID output of specific buggy program(s)'

COMMAND_SIMFIX = '--simfix'
COMMAND_SIMFIX_DESC = 'Execute PreSimFix.jar against specific buggy program(s)'

COMMAND_CLEAR = '--clear'
COMMAND_CLEAR_DESC = 'Clear all repositories\' jaid_ouput under the buggy_repo folder'

COMMAND_BACKUP = '--backup'
COMMAND_BACKUP_DESC = 'Backup all repositories\' jaid_ouput under the buggy_repo folder'

COMMAND_RM_BACKUP = '--rm_backup'
COMMAND_RM_BACKUP_DESC = 'Backup all repositories\' jaid_ouput under the buggy_repo folder'

Commands = {
    COMMAND_SHOW_INFO: COMMAND_SHOW_INFO_DESC,
    COMMAND_SHOW_CONFIG: COMMAND_SHOW_CONFIG_DESC,
    COMMAND_BACKUP: COMMAND_BACKUP_DESC,
    COMMAND_RM_BACKUP: COMMAND_RM_BACKUP_DESC,

    COMMAND_PREPARE: COMMAND_PREPARE_DESC,
    COMMAND_RUN: COMMAND_RUN_DESC,
    COMMAND_READ: COMMAND_READ_DESC,
    COMMAND_SIMFIX: COMMAND_SIMFIX_DESC,
    COMMAND_CLEAR: COMMAND_CLEAR_DESC,

    COMMAND_SET_USED_JAR: COMMAND_SET_USED_JAR_DESC,
    COMMAND_SET_SBFL: COMMAND_SET_SBFL_DESC,
    COMMAND_SET_RANKING: COMMAND_SET_RANKING_DESC,
    COMMAND_SET_MAX_TEST: COMMAND_SET_MAX_TEST_DESC,
    COMMAND_SET_ICJ_BLACK_TEST: COMMAND_SET_ICJ_BLACK_TEST_DESC,
    COMMAND_SET_D4J: COMMAND_SET_D4J_DESC,
    COMMAND_SET_PROCESS_DEV_FIX: COMMAND_SET_PROCESS_DEV_FIX_DESC,
    COMMAND_SET_RANK_MODEL: COMMAND_SET_RANK_MODEL_DESC,
    COMMAND_SET_TWO_MODELS: COMMAND_SET_TWO_MODELS,
    COMMAND_UPDATE_MODEL: COMMAND_UPDATE_MODEL_DESC,
    COMMAND_TIME_LIMIT: COMMAND_TIME_LIMIT_DESC,
    COMMAND_MODEL_PREPARE: COMMAND_MODEL_PREPARE_DESC,
    COMMAND_SIMFIX_MODE: COMMAND_SIMFIX_MODE_DESC,
    COMMAND_PATCH_NUM: COMMAND_PATCH_NUM_DESC,
}

D4J_OLD_CANDIDATES = {
    'commons-math82': '0B3Jlt8l7iCKAakREc3lHbmtUSVE',
    'commons-math105': '0B3Jlt8l7iCKAbFFua3d6amZSdlE',
    'commons-math5': '0B3Jlt8l7iCKAbUw4emZHSERiZXc',
    'joda-time19': '0B3Jlt8l7iCKAX3A3MWRvallNamc',
    'closure-compiler63': '0B3Jlt8l7iCKAU1ktZTlRYlNFN2c',
    'commons-lang38': '0B3Jlt8l7iCKAVjJNSXpwSUdzek0',
    'closure-compiler5': '0B3Jlt8l7iCKAaGlHQnJIbk52S0E',
    'commons-math101': '0B3Jlt8l7iCKAdUx4TmFfUGRYTzg',
    'closure-compiler118': '0B3Jlt8l7iCKAbWp0MkFGcEZfYm8',
    'closure-compiler86': '0B3Jlt8l7iCKAZ0lmTmlYYUxESG8',
    'commons-math80': '0B3Jlt8l7iCKASUZIX3FweGZPX2c',
    'JFreechart24': '0B3Jlt8l7iCKAalA0cHRxRmM3dE0',
    'commons-math85': '0B3Jlt8l7iCKAZHZlV1oxMHRhVG8',
    'JFreechart9': '0B3Jlt8l7iCKAUEhtWUlmaDh0Rlk',
    'closure-compiler125': '0B3Jlt8l7iCKAOVdoUVNQemRFVGM',
    'commons-lang45': '0B3Jlt8l7iCKAU093N0Q3MVg2bm8',
    'commons-lang24': '0B3Jlt8l7iCKAb3RUS09OODdLbVE',
    'closure-compiler1': '0B3Jlt8l7iCKASE9jN1dseERZdVU',
    'closure-compiler113': '0B3Jlt8l7iCKAZHpFbEhyMG8tcDA',
    'closure-compiler33': '0B3Jlt8l7iCKASkQ2OFZKdjRQQTA',
    'closure-compiler70': '0B3Jlt8l7iCKAaUtTNkFidUpsbjg',
    'commons-math50': '0B3Jlt8l7iCKANlgzUXVrWUpJdmc',
    'JFreechart1': '0B3Jlt8l7iCKASFZpUW5BWndnRGM',
    'closure-compiler104': '0B3Jlt8l7iCKAdm5mRVNMbEV6UFE',
    'closure-compiler10': '0B3Jlt8l7iCKAUV9UN3NUWm9jVFE',
    'commons-math53': '0B3Jlt8l7iCKATkJBa2Z1bjdYdmM',
    'commons-math32': '0B3Jlt8l7iCKAWVpmWnpraDQwclk',
    'closure-compiler73': '0B3Jlt8l7iCKAYnNPSzE2dVNERDQ',
    'JFreechart26': '0B3Jlt8l7iCKAaFJqSjhrUkRaMkU',
    'commons-lang55': '0B3Jlt8l7iCKAejM5b05FUUJLNmM',
    'closure-compiler126': '0B3Jlt8l7iCKAaFVoS2lyUTc3Ujg',
    'commons-lang61': '0B3Jlt8l7iCKAMHlUV2JYYVdZYXM',
    'commons-lang33': '0B3Jlt8l7iCKAZzJfRHQ1bVk0Wlk',
    'commons-math70': '0B3Jlt8l7iCKARnQ0alNFQVMwSk0',
    'closure-compiler40': '0B3Jlt8l7iCKAcDdBNVpsSW1vaEU',
    'closure-compiler31': '0B3Jlt8l7iCKAOGN1TFBmaVNBMUE',
    'closure-compiler18': '0B3Jlt8l7iCKAdUxwZ0F0TnR6RWs',
    'commons-lang51': '0B3Jlt8l7iCKAaHZEVzI4Yk5Yb00',
    'commons-lang39': '0B3Jlt8l7iCKASlNNVzBtUzk4Q1k',
}

D4J_NEW_CANDIDATES = {
    'commons-math25': '0B3Jlt8l7iCKAbm02X2xGMWdNQjA',
    'commons-math82': '0B3Jlt8l7iCKAUUxBdFBRSGNUeHM',
    'commons-math99': '0B3Jlt8l7iCKAM3lhQTlQbVZfM0E',
    'commons-math42': '0B3Jlt8l7iCKAc1lycGktTzNHMWs',
    'commons-lang7': '0B3Jlt8l7iCKAQjEtbGo5VzJRSlE',
    'joda-time4': '0B3Jlt8l7iCKATVRreThNbkFoYWM',
    'commons-math33': '0B3Jlt8l7iCKAUnZBajRiU3F6aTA',
    'commons-math105': '0B3Jlt8l7iCKAejAwY1dtV1pXLVU',
    'commons-lang10': '0B3Jlt8l7iCKAc3gzZjhzSEdKdHc',
    'commons-lang53': '0B3Jlt8l7iCKALXJ6OUlWVzNOdlE',
    'commons-math5': '0B3Jlt8l7iCKAUVhDWTVHeDJzd28',
    'commons-math8': '0B3Jlt8l7iCKAT0x2c010WkFBNms',
    'commons-lang46': '0B3Jlt8l7iCKAV1pxTGktODZ3LUk',
    'joda-time19': '0B3Jlt8l7iCKAR3hHMjd4cnMwYXc',
    'commons-math2': '0B3Jlt8l7iCKANnVrbTlOQU5CMFE',
    'commons-lang38': '0B3Jlt8l7iCKARmh3SnVzTFRXcXc',
    'JFreechart7': '0B3Jlt8l7iCKAdmJWWkFWTWxxUVk',
    'commons-lang6': '0B3Jlt8l7iCKAX3g0MFFhU21fZGc',
    'commons-math40': '0B3Jlt8l7iCKAS0FwaUZTcTZqalU',
    'commons-math3': '0B3Jlt8l7iCKAWUlmUkIzZW51NDQ',
    'JFreechart13': '0B3Jlt8l7iCKAS1VieUNKUW9idXM',
    'commons-math49': '0B3Jlt8l7iCKAYTVuNUlqVHB2OUE',
    'commons-math80': '0B3Jlt8l7iCKAS3BSQnk1VUtIVzQ',
    'JFreechart15': '0B3Jlt8l7iCKANkNVNjA0YW1IUXM',
    'commons-math34': '0B3Jlt8l7iCKARGppdTYxU0wyejQ',
    'commons-math89': '0B3Jlt8l7iCKAd21OMnFBTjJKSG8',
    'JFreechart24': '0B3Jlt8l7iCKAMVk3MmZRZ19DMGM',
    'commons-math87': '0B3Jlt8l7iCKAc1JCNzhBR3FLVjg',
    'commons-math85': '0B3Jlt8l7iCKASEUtUDN4ZWMwYTA',
    'JFreechart9': '0B3Jlt8l7iCKAcVB5WGloRmg3MzQ',
    'closure-compiler125': '0B3Jlt8l7iCKAdXg3ZGUtanJnamM',
    'commons-lang45': '0B3Jlt8l7iCKASE1WQUM2RGJ4NnM',
    'commons-lang24': '0B3Jlt8l7iCKAZVBiZGF0MUtMdUk',
    'commons-math69': '0B3Jlt8l7iCKAOEIwdHhublp5VEE',
    'commons-math84': '0B3Jlt8l7iCKAU1FKWEJoMmEyX2s',
    'commons-lang57': '0B3Jlt8l7iCKAVlJkbTZwUTFDaU0',
    'JFreechart21': '0B3Jlt8l7iCKAMlhzblFqQUQxMTg',
    'commons-math57': '0B3Jlt8l7iCKANDkzdkJVVlRfSFE',
    'commons-lang43': '0B3Jlt8l7iCKATDFGNVpsdWdFcnc',
    'closure-compiler33': '0B3Jlt8l7iCKASkZoaDJISkZfZUE',
    'closure-compiler70': '0B3Jlt8l7iCKAMmJEcmtHTDQyV0k',
    'commons-lang59': '0B3Jlt8l7iCKANVR6dVUyYmRJYzA',
    'commons-math93': '0B3Jlt8l7iCKAY09aQ2Y2bUttODA',
    'commons-math50': '0B3Jlt8l7iCKAbWNzc0V3eFhiUjg',
    'commons-math28': '0B3Jlt8l7iCKATjBUd0t3Zjl2S0U',
    'JFreechart1': '0B3Jlt8l7iCKAM3UtMnJsOWZ4TUU',
    'closure-compiler10': '0B3Jlt8l7iCKAajNoSG1KVmYzVW8',
    'joda-time15': '0B3Jlt8l7iCKAYWhmWmZqT20zNnM',
    'commons-math95': '0B3Jlt8l7iCKAaGpFbmYtOFIyS00',
    'commons-math53': '0B3Jlt8l7iCKAUW1ybmNKQkJydWs',
    'commons-math32': '0B3Jlt8l7iCKAbXUzWGQ1RDZLQjA',
    'closure-compiler73': '0B3Jlt8l7iCKANHN2TVhvQmx3S0U',
    'commons-math58': '0B3Jlt8l7iCKATy1yMXVid21SVWc',
    'JFreechart5': '0B3Jlt8l7iCKAWk1GMnpwc3BFSjA',
    'closure-compiler62': '0B3Jlt8l7iCKATXFIQ2trNTBhZDQ',
    'JFreechart26': '0B3Jlt8l7iCKAbmdwTW1Ya0pGRmM',
    'commons-lang55': '0B3Jlt8l7iCKAZE95bjM3T0NLdk0',
    'closure-compiler14': '0B3Jlt8l7iCKAdHFkNWVzTVBVV28',
    'closure-compiler126': '0B3Jlt8l7iCKAMUZhcm83bjcwWEk',
    'closure-compiler120': '0B3Jlt8l7iCKAZDdWc2VKV3pTVU0',
    'closure-compiler66': '0B3Jlt8l7iCKAUU54YTJhYWh1NzA',
    'closure-compiler15': '0B3Jlt8l7iCKAakRlb2NtNy1QdWs',
    'commons-math81': '0B3Jlt8l7iCKASjhiWDNBRG0yZ00',
    'closure-compiler51': '0B3Jlt8l7iCKAenpLeGJBUG5DM1U',
    'commons-math90': '0B3Jlt8l7iCKAcDhmemswSTBMWUE',
    'commons-math88': '0B3Jlt8l7iCKAZHdfTERvYzhJUzA',
    'commons-math78': '0B3Jlt8l7iCKAVDJUNExUUmpyd00',
    'commons-lang58': '0B3Jlt8l7iCKAZ3hRenVDbm1BMTg',
    'commons-math35': '0B3Jlt8l7iCKAU19jc3FyN1N3NVU',
    'commons-lang33': '0B3Jlt8l7iCKAbWZZcWwyQkNBNTA',
    'commons-math73': '0B3Jlt8l7iCKASmoxVnNZWnNLeFk',
    'commons-math70': '0B3Jlt8l7iCKAQl93Q2h1VTV2NHc',
    'JFreechart3': '0B3Jlt8l7iCKAV182OFVPcmJnNnc',
    'closure-compiler40': '0B3Jlt8l7iCKAYW5MQXRhUGVFcms',
    'commons-math97': '0B3Jlt8l7iCKASmh5cEtkQUxLSDQ',
    'JFreechart25': '0B3Jlt8l7iCKAUHNFTFFaYjlOS3M',
    'JFreechart8': '0B3Jlt8l7iCKAeHNodGRnNC00dDA',
    'JFreechart14': '0B3Jlt8l7iCKAREpUdEcxdTI5dFE',
    'commons-lang35': '0B3Jlt8l7iCKAM0VvdXhuc0E4Y3c',
    'JFreechart19': '0B3Jlt8l7iCKAWjdhNW5rbEpYMzg',
    'commons-lang44': '0B3Jlt8l7iCKARW8tUzZlVjBFamM',
    'commons-lang51': '0B3Jlt8l7iCKAZnRWNGUzUUdtYVU',
    'commons-lang39': '0B3Jlt8l7iCKAM0NLU3FxS2FhQmc',
    'mockito38': '0B3Jlt8l7iCKALTk3VUNxb2Z5TzQ',
    'mockito24': '0B3Jlt8l7iCKAZFBWaW1DeUg5T1E',
    'mockito1': '0B3Jlt8l7iCKAR0QtSlZ2UDFUZHc',
    'mockito7': '0B3Jlt8l7iCKAR0dZY2dDSjdUMjQ',
    'mockito29': '0B3Jlt8l7iCKAd0lhT0VpOUpqMUE',
    'mockito28': '0B3Jlt8l7iCKAX0lXa0E5V2dDdGs',
    'mockito33': '0B3Jlt8l7iCKANklEOTczM2t3bGc',
    'mockito22': '0B3Jlt8l7iCKAaE5RQ1FFSlVlT0E',
    'mockito11': '0B3Jlt8l7iCKAaXRlLUNkRER5Qlk',
    'mockito12': '0B3Jlt8l7iCKAQkt3ZUNwZUtaNjA',
    'mockito18': '0B3Jlt8l7iCKAcUpKdTZPU2lLWFk',
    'mockito3': '0B3Jlt8l7iCKAeGpOQWxoRG12VFU',
    'mockito21': '0B3Jlt8l7iCKAUzhNYV9NVlNFNXM',
    'mockito5': '0B3Jlt8l7iCKAV25Za2xPUHN6elU',
    'mockito34': '0B3Jlt8l7iCKAMjVRX21ESDhPQU0',
    'mockito6': '0B3Jlt8l7iCKAdEwwYzFUeURUVEk',
    'mockito4': '0B3Jlt8l7iCKAQ0NVVi1WQUNFVEk',
    'mockito37': '0B3Jlt8l7iCKARldNaVc3a1NUcm8',
    'mockito8': '0B3Jlt8l7iCKAbklkb3duOEg1ZTQ',
    'mockito13': '0B3Jlt8l7iCKAQWhpUVM2MGtSMFE',
    'mockito27': '0B3Jlt8l7iCKAWVY5UXh0cnc4M3M',
    'mockito20': '0B3Jlt8l7iCKAZG5MMDNwOGctRW8',
    'mockito35': '0B3Jlt8l7iCKARFRzMjlfZExzVGM',
}
D4J_CANDIDATES = D4J_OLD_CANDIDATES.copy()
D4J_CANDIDATES.update(D4J_NEW_CANDIDATES)

ICJ_CANDIDATES = {
    'checksum': '0B3Jlt8l7iCKAZU5iekpSZ28wWW8',
    'digits': '0B3Jlt8l7iCKAZWU3MldaUmI5TW8',
    'grade': '0B3Jlt8l7iCKAdGFYdWdaMEo1OTA',
    'median': '0B3Jlt8l7iCKAMTk0aDFleXpJZEE',
    'smallest': '0B3Jlt8l7iCKAbVp5QURoZWVNWFU',
    'syllables': '0B3Jlt8l7iCKAdDRNcFQzUEhjREU',
}

QUIX_BUGS_GIT = 'https://github.com/chenliushan/QuixBugs.git'
QUIX_BUGS_REPO_NAME = 'QuixBugs'
QUIX_BUGS_BV_LIST = ['bitcount', 'breadth_first_search', 'bucketsort', 'depth_first_search', 'detect_cycle',
                     'find_first_in_sorted', 'find_in_sorted', 'flatten', 'gcd', 'get_factors', 'hanoi',
                     'is_valid_parenthesization', 'kheapsort', 'kth', 'lcs_length', 'levenshtein', 'lis',
                     'longest_common_subsequence', 'max_sublist_sum', 'mergesort', 'minimum_spanning_tree',
                     'next_palindrome', 'next_permutation', 'pascal', 'possible_change', 'powerset', 'quicksort',
                     'reverse_linked_list', 'rpn_eval', 'shortest_path_length', 'shortest_path_lengths',
                     'shortest_paths', 'shunting_yard', 'sieve', 'sqrt', 'subsequences', 'to_base',
                     'topological_ordering', 'knapsack', 'wrap']

JAID_JAR = "1yamvHTKlty_c3M0nNZikqNEawcRg_bre"
