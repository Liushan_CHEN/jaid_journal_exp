import json
import os
import shutil
import subprocess
import sys
from abc import ABC, abstractmethod
from distutils.dir_util import copy_tree
from shutil import copyfile

import constants
from download_google_file import download_file_from_google_drive
from jaid_properties import JaidProperties, update_local_lib
from read_fj_output import read_new_jaid_result, append_csv
from utils import unzip, split_src_num, split_icj_repo_bugid, split_icj_bug_id


class AbsBenchMark(ABC):
    repository_path = ''

    def __init__(self, exp_config, bv):
        self.exp_config = exp_config
        self.bv = bv

    @abstractmethod
    def prepare_bv(self):
        pass

    @abstractmethod
    def run_bv(self):
        pass

    @abstractmethod
    def read_bv(self):
        pass

    def set_models(self, jaid_properties, repo_name, bug_id):
        default_model = 'default'
        if len(self.exp_config.rank_model):
            model_path = os.path.join(
                self.exp_config.get_model_folder_path(),
                self.exp_config.rank_model + '-' + repo_name + str(bug_id) + '.bin')
            feature_path = os.path.join(
                self.exp_config.get_model_folder_path(),
                self.exp_config.rank_model + '-' + repo_name + str(bug_id) + '-feature.txt')
            if not os.path.isfile(model_path) or not os.path.isfile(feature_path):
                model_path = os.path.join(
                    self.exp_config.get_model_folder_path(),
                    self.exp_config.rank_model + '-' + default_model + '.bin')
                feature_path = os.path.join(
                    self.exp_config.get_model_folder_path(),
                    self.exp_config.rank_model + '-' + default_model + '-feature.txt')
            jaid_properties.value_map[JaidProperties.p_RankingModel] = model_path
            jaid_properties.value_map[JaidProperties.p_FeatureMap] = feature_path
        if self.exp_config.two_models:
            model_path = os.path.join(
                self.exp_config.get_model_folder_path(), 'correct-' + repo_name + str(bug_id) + '.bin')
            feature_path = os.path.join(
                self.exp_config.get_model_folder_path(), 'correct-' + repo_name + str(bug_id) + '-feature.txt')
            if not os.path.isfile(model_path) or not os.path.isfile(feature_path):
                model_path = os.path.join(
                    self.exp_config.get_model_folder_path(), 'correct-' + default_model + '.bin')
                feature_path = os.path.join(
                    self.exp_config.get_model_folder_path(), 'correct-' + default_model + '-feature.txt')
            jaid_properties.value_map[JaidProperties.p_KilledModel] = model_path
            jaid_properties.value_map[JaidProperties.p_KilledFeatureMap] = feature_path
        jaid_properties.value_map[JaidProperties.p_UpdateModel] = str(self.exp_config.update_model)
        jaid_properties.value_map[JaidProperties.p_ModelPrepare] = str(self.exp_config.model_prepare)


def download_d4j_icj_if_not_exist(repository_path):
    if not os.path.exists(repository_path):
        zip_repo_path = repository_path + constants.ZIP_SUFFIX
        if not os.path.exists(zip_repo_path):
            # download the specific project and store in 'zip_repo_path'
            zip_file = os.path.split(zip_repo_path)
            zip_file_name = zip_file[1][0:len(zip_file[1]) - len(constants.ZIP_SUFFIX)]
            if zip_file_name in constants.ICJ_CANDIDATES.keys():
                download_file_from_google_drive(constants.ICJ_CANDIDATES[zip_file_name], zip_repo_path)
            else:
                print('download ' + zip_file_name + ' fail.')
                return
        unzip(zip_repo_path, repository_path)


def run_old(exp_config, properties_file_path):
    paths = os.path.split(properties_file_path)
    if not os.path.isfile(exp_config.get_jar_path()):
        print('Jaid.jar is not exitst!')
        return
    command = repr(os.path.normpath(os.path.join(exp_config.JDKDir, 'bin', 'java'))).replace('\'', '"') + \
              ' -agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=22018' + \
              ' -d64 -Xms12g -Xmx14g -jar ' + exp_config.get_jar_path() + \
              ' ' + constants.JAID_ARG_PROPERTY + ' ' + properties_file_path
    print('EXE COMMAND: ' + command)
    os.system(command)
    print('FINISH EXECUTING COMMAND: ' + command)


def run_(exp_config, properties_file_path):
    if not os.path.isfile(exp_config.get_jar_path()):
        print('Jaid.jar is not exitst!')
        return
    wdir, _ = os.path.split(properties_file_path)
    os.chdir(wdir)
    command = [repr(os.path.normpath(os.path.join(exp_config.JDKDir, 'bin', 'java'))).replace('\'', '')
        , '-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=22018', '-d64', '-Xms12g', '-Xmx14g', '-jar',
               exp_config.get_jar_path(), constants.JAID_ARG_PROPERTY, properties_file_path]
    comand_str = ' '.join(command)
    process = subprocess.Popen(command)
    try:
        print(str(process.pid) + ' EXE COMMAND: ' + comand_str)
        process.wait(exp_config.time_limit * 60)
    except subprocess.TimeoutExpired:
        print(str(exp_config.time_limit * 60) + ' min timed out - killing', process.pid)
        process.kill()
    print('FINISH EXECUTING COMMAND: ' + comand_str)


def get_d4j_repo_path(exp_config, repo_name, bug_id):
    repo = constants.repositories[repo_name.lower()] + bug_id
    return repo, os.path.join(exp_config.get_working_path(), repo)


class D4jBenchMark(AbsBenchMark):
    RELEVANT_TESTS_FILE_NAME = 'out_relevant_tests.txt'
    MTF_FILE = 'd4j_mtf.txt'
    DEFAULT_LIBS = {
        'Lang': 'ProjectLib = '
                '/root/exp_tools/defects4j/framework/lib/junit-4.12.jar:'
                '/root/exp_tools/defects4j/framework/projects/Lang/lib/org/easymock/easymock/2.5.2/easymock-2.5.2.jar:'
                '/root/exp_tools/defects4j/framework/projects/Lang/lib/cglib.jar:'
                '/root/exp_tools/defects4j/framework/projects/Lang/lib/asm.jar:'
                '/root/exp_tools/defects4j/framework/projects/Lang/lib/easymock.jar:'
                '/root/exp_tools/defects4j/framework/projects/Lang/lib/commons-io.jar',
        'Closure': 'ProjectLib ='
                   'build/classes:build/lib/rhino.jar:lib/args4j.jar:lib/guava.jar:lib/json.jar:lib/jsr305.jar:'
                   'lib/objenesis.jar:lib/mockito-core.jar:'
                   'lib/jarjar.jar:lib/libtrunk_rhino_parser_jarjared.jar:lib/protobuf-java.jar:lib/ant-launcher.jar:'
                   'lib/ant.jar:lib/caja-r4314.jar:/root/exp_tools/defects4j/framework/lib/junit-4.12.jar:'
                   'lib/ant_deploy.jar:lib/args4j_deploy.jar:lib/guava-r06.jar:lib/protobuf-java-2.3.0.jar:'
                   'lib/protobuf_deploy.jar:lib/google_common_deploy.jar:'
                   'lib/hamcrest-core-1.1.jar',
        'Math': 'ProjectLib = '
                '/root/exp_tools/defects4j/framework/lib/junit-4.12.jar:'
                '/root/exp_tools/defects4j/framework/projects/Math/lib/commons-discovery-0.5.jar',
        'Chart': 'ProjectLib = '
                 'lib/servlet.jar:/root/exp_tools/defects4j/framework/lib/junit-4.12.jar:build',
        'Time': 'ProjectLib = '
                'build/classes:/root/exp_tools/defects4j/framework/projects/Time/lib/joda-convert-1.2.jar:'
                '/root/exp_tools/defects4j/framework/lib/junit-4.12.jar:build/classes:target/classes',
        'Mockito': 'ProjectLib = '
                   'build/classes/main:target/classes:/root/exp_tools/defects4j/framework/lib/junit-4.12.jar:'
                   '/root/exp_tools/defects4j/framework/projects/Mockito/lib/asm-all-5.0.4.jar:'
                   '/root/exp_tools/defects4j/framework/projects/Mockito/lib/assertj-core-2.1.0.jar:'
                   '/root/exp_tools/defects4j/framework/projects/Mockito/lib/cglib-and-asm-1.0.jar:'
                   '/root/exp_tools/defects4j/framework/projects/Mockito/lib/cobertura-2.0.3.jar:'
                   '/root/exp_tools/defects4j/framework/projects/Mockito/lib/fest-assert-1.3.jar:'
                   '/root/exp_tools/defects4j/framework/projects/Mockito/lib/fest-util-1.1.4.jar:'
                   '/root/exp_tools/defects4j/framework/projects/Mockito/lib/hamcrest-all-1.3.jar:'
                   '/root/exp_tools/defects4j/framework/projects/Mockito/lib/hamcrest-core-1.1.jar:'
                   '/root/exp_tools/defects4j/framework/projects/Mockito/lib/objenesis-2.1.jar:'
                   '/root/exp_tools/defects4j/framework/projects/Mockito/lib/objenesis-2.2.jar:'
                   '/root/exp_tools/defects4j/framework/projects/Mockito/lib/powermock-reflect-1.2.5.jar'
    }
    DEFAULT_SRC = {
        'Lang': 'src/main/java',
        'Closure': 'src',
        'Chart': 'source',
        'Math': 'src/main/java',
        'Time': 'src/main/java',
        'Mockito': 'src'
    }
    DEFAULT_TEST_SRC = {'Lang': 'src/test/java',
                        'Closure': 'test',
                        'Chart': 'tests',
                        'Math': 'src/test/java',
                        'Time': 'src/test/java',
                        'Mockito': 'test'}

    def __init__(self, exp_config, bv):
        super().__init__(exp_config, bv)
        self.repo_name, self.bug_id = split_src_num(self.bv)
        self.repo, self.repository_path = get_d4j_repo_path(self.exp_config, self.repo_name, self.bug_id)

    def __checkout_repo(self, is_buggy):
        if not os.path.exists(self.repository_path):
            repo_path = os.path.join('/tmp', 'D4J-' + self.repo_name)
            if is_buggy:
                version = 'b'
            else:
                version = 'f'
            command = self.exp_config.get_d4j_exe() + ' checkout -p ' + self.repo_name + ' -v ' + self.bug_id \
                      + version + ' -w ' + repo_path
            print(command)
            os.system(command)
            if os.path.exists(repo_path):
                prevdir = os.getcwd()
                os.chdir(repo_path)
                os.system(self.exp_config.get_d4j_exe() + ' compile')
                os.chdir(prevdir)
                copy_tree(repo_path, self.repository_path)

    def __get_relevant_tests(self):
        save_dir = os.getcwd()
        output = os.path.join(self.repository_path, self.RELEVANT_TESTS_FILE_NAME)
        if not os.path.isfile(output):
            os.chdir(self.repository_path)
            command = self.exp_config.get_d4j_exe() + ' export -p tests.relevant -o ' + output
            print(command)
            os.system(command)
        else:
            print('Exporting_test not executed, ' + output + ' is exist.')
        relevan_tests = os.pathsep.join([line.rstrip() for line in open(output)])
        os.chdir(save_dir)
        return relevan_tests

    def __get_d4j_mtf(self):
        qb_mtf_path = os.path.join(self.exp_config.get_lib_path(), self.MTF_FILE)

        with open(qb_mtf_path) as mtf_file:
            mtfs = mtf_file.readlines()
            for line in mtfs:
                if line.startswith(self.repo_name + self.bug_id + ': '):
                    mtf = line[len(self.repo_name + self.bug_id + ': '):]
                    return mtf.strip()

    def get_d4j_libs(self):
        libs = self.DEFAULT_LIBS[self.repo_name]
        libs = update_local_lib(libs, self.exp_config, self.repository_path)
        return libs

    def prepare_bv(self):
        drive, repo = os.path.split(self.repository_path)
        should_update_property_file = False
        self.__checkout_repo(True)
        back_up_properties_file_path = os.path.join(self.exp_config.get_properties_folder_path(),
                                                    repo + '_' + JaidProperties.PROPERTY_FILE_NAME)
        if os.path.isfile(back_up_properties_file_path):
            old_property_file_path = back_up_properties_file_path
        else:
            old_property_file_path = os.path.join(self.repository_path, JaidProperties.PROPERTY_FILE_NAME)

        jaid_properties = JaidProperties(self.exp_config, self.repository_path)
        if os.path.isfile(old_property_file_path):
            jaid_properties.read_property_from_file(old_property_file_path)
        else:
            jaid_properties.value_map[JaidProperties.p_ProjectLib] = self.get_d4j_libs()
            jaid_properties.value_map[JaidProperties.p_MethodToFix] = self.__get_d4j_mtf()
            jaid_properties.value_map[JaidProperties.p_ProjectSourceDir] = self.DEFAULT_SRC[self.repo_name]
            jaid_properties.value_map[JaidProperties.p_ProjectTestSourceDir] = self.DEFAULT_TEST_SRC[self.repo_name]

        if self.repo_name == 'Lang':
            jaid_properties.value_map[JaidProperties.p_Encoding] = 'ISO-8859-1'
            if int(self.bug_id) >= 36:
                jaid_properties.value_map[JaidProperties.p_ProjectSourceDir] = 'src/java'
                jaid_properties.value_map[JaidProperties.p_ProjectTestSourceDir] = 'src/test'
                jaid_properties.value_map[JaidProperties.p_TargetJavaVersion] = '1.3'
                if int(self.bug_id) == 39 or int(self.bug_id) == 38:
                    jaid_properties.value_map[JaidProperties.p_TargetJavaVersion] = '1.5'
        elif self.repo_name == 'Math':
            jaid_properties.value_map[JaidProperties.p_Encoding] = 'ISO-8859-1'
            if int(self.bug_id) >= 85:
                jaid_properties.value_map[JaidProperties.p_ProjectSourceDir] = 'src/java'
                jaid_properties.value_map[JaidProperties.p_ProjectTestSourceDir] = 'src/test'
        elif self.repo_name == 'Time':
            jaid_properties.value_map[JaidProperties.p_Encoding] = 'ISO-8859-1'
        elif self.repo_name == 'Closure':
            jaid_properties.value_map[JaidProperties.p_Encoding] = 'UTF-8'
        elif self.repo_name == 'Mockito':
            jaid_properties.value_map[JaidProperties.p_TargetJavaVersion] = '1.5'

        if self.exp_config.process_dev_fix:
            jaid_properties.value_map[JaidProperties.p_DevFixFile] = os.path.join(
                self.exp_config.get_dev_fix_folder_path(), self.repo_name + str(self.bug_id) + '.java')
        self.set_models(jaid_properties, self.repo_name, self.bug_id)

        # Using D4J relevant test only
        if len(jaid_properties.value_map[JaidProperties.p_ProjectTestsToInclude].strip()) == 0:
            should_update_property_file = True
            jaid_properties.value_map[JaidProperties.p_ProjectTestsToInclude] = self.__get_relevant_tests()
        property_file_path = jaid_properties.write_default_property_file()
        if not os.path.isfile(old_property_file_path) or should_update_property_file:
            copyfile(property_file_path, back_up_properties_file_path)
        print('The buggy program ' + repo + ' is ready. Program arguments of JAID running configuration is:')
        print(constants.JAID_ARG_PROPERTY + ' ' + property_file_path)
        return property_file_path

    def run_bv(self):
        properties_file_path = self.prepare_bv()
        run_(self.exp_config, properties_file_path)

    def read_bv(self):
        for inner_inner_folder in os.listdir(self.repository_path):
            if inner_inner_folder == constants.OUTPUT_FOLDER_NAME:  # find the experiment output in repo
                jaid_output_dir = os.path.join(self.repository_path, inner_inner_folder)
                read_new_jaid_result(jaid_output_dir)


class IcjBenchmark(AbsBenchMark):
    def __init__(self, exp_config, bv):
        super().__init__(exp_config, bv)
        if '-' in bv:  # icj repo bug id
            repo, bug_id = split_icj_repo_bugid(bv)
            if repo and bug_id:
                self.repo_cluster_path = os.path.join(exp_config.get_working_path(), repo)
                self.author_id, self.icj_bv = split_icj_bug_id(bug_id)

    def prepare_bv(self):
        download_d4j_icj_if_not_exist(self.repo_cluster_path)
        for folder in os.listdir(self.repo_cluster_path):
            if folder.startswith(self.author_id):
                self.repository_path = os.path.join(self.repo_cluster_path, folder, self.icj_bv)
                if os.path.exists(self.repository_path):

                    jaid_properties = JaidProperties(self.exp_config, self.repository_path)
                    libs = os.path.pathsep.join([self.exp_config.get_junit_path(), self.exp_config.get_hamcrest_path()])
                    jaid_properties.value_map[JaidProperties.p_ProjectLib] = libs
                    jaid_properties.value_map[JaidProperties.p_MethodToFix] = self.__get_icj_mtf()
                    if self.exp_config.IcjBlackBoxOnly:
                        jaid_properties.value_map[
                            JaidProperties.p_ProjectTestsToInclude] = self.get_black_box_test_class()
                        jaid_properties.value_map[
                            jaid_properties.p_EnableSecondValidation] = 'true'
                    self.set_models(jaid_properties, self.bv, '')
                    property_file_path = jaid_properties.write_default_property_file()
                    if property_file_path:
                        print(constants.JAID_ARG_PROPERTY + ' ' + property_file_path)
                    return property_file_path

    def run_bv(self):
        bv_properties = self.prepare_bv()
        if bv_properties:
            run_(self.exp_config, bv_properties)

    def read_bv(self):
        csv_path = os.path.join(self.exp_config.get_working_path(), constants.ICJ_RESULT_FILE_NAME)
        for author_id_folder in os.listdir(self.repo_cluster_path):
            if author_id_folder.startswith(self.author_id):
                repository_path = os.path.join(self.repo_cluster_path, author_id_folder, self.icj_bv)
                for inner_inner_folder in os.listdir(repository_path):
                    if inner_inner_folder == constants.OUTPUT_FOLDER_NAME:  # find the experiment output in repo
                        jaid_output_dir = os.path.join(repository_path, inner_inner_folder)
                        result = read_new_jaid_result(jaid_output_dir)
                        if result:
                            csv_row = [self.bv]
                            csv_row.extend(result)
                            append_csv(csv_path, csv_row)

    sourcecode_dir = ['src', 'main', 'java', 'introclassJava']
    test_dir = ['src', 'test', 'java', 'introclassJava']

    def __get_icj_mtf(self):
        src_path = os.path.join(self.repository_path, os.path.sep.join(self.sourcecode_dir))
        print('repository_path: ' + self.repository_path)
        if os.path.exists(src_path):
            for a_file in os.listdir(src_path):
                if a_file.endswith('.java'):
                    class_name = a_file[0:len(a_file) - 5]
                    return 'exec()@introclassJava.' + class_name

    def get_black_box_test_class(self):
        src_path = os.path.join(self.repository_path, os.path.sep.join(self.test_dir))
        if os.path.exists(src_path):
            for a_file in os.listdir(src_path):
                if a_file.endswith('BlackboxTest.java'):
                    class_name = a_file[0:len(a_file) - 5]
                    return 'introclassJava.' + class_name


class QbBenchMark(AbsBenchMark):
    src_pkg = "java_programs"
    test_pkg = ["java_testcases", "junit"]
    QUIX_BUGS_BUGGY_SUFFIX = '_bq'
    QUIX_BUGS_TEST_SUFFIX = '_TEST'

    QUIXBUGS_MTF_FILE = 'qb_mtf.txt'

    TEST_HELPER = 'QuixFixOracleHelper.java'
    WEIGHTEDEDGE_CLASS = 'WeightedEdge.java'
    NODE_CLASS = 'Node.java'

    def __init__(self, exp_config, bv):
        super().__init__(exp_config, bv)
        self.class_name = bv[len(constants.QB_PREFIX):]

    def prepare_bv(self):
        working_path = self.exp_config.get_working_path()
        repo_path = self.get_repo_path()
        if not os.path.exists(repo_path):  # download QuixBugs from my GitHub
            os.system("git clone " + constants.QUIX_BUGS_GIT + ' ' + repo_path)
        # construct project structure for a bv
        src_name = self.class_name.upper() + constants.JAVA_SUFFIX
        test_name = self.class_name.upper() + self.QUIX_BUGS_TEST_SUFFIX + constants.JAVA_SUFFIX
        o_source_path = os.path.join(repo_path, self.src_pkg, src_name)
        o_weighted_edge_path = os.path.join(repo_path, self.src_pkg, self.WEIGHTEDEDGE_CLASS)
        o_node_path = os.path.join(repo_path, self.src_pkg, self.NODE_CLASS)

        o_test_path = os.path.join(repo_path, os.path.sep.join(self.test_pkg), test_name)
        o_test_helper_path = os.path.join(repo_path, os.path.sep.join(self.test_pkg), self.TEST_HELPER)

        if os.path.isfile(o_source_path) and os.path.isfile(o_test_path):
            # check program folder
            bv_program_path = os.path.join(working_path, self.class_name.lower() + self.QUIX_BUGS_BUGGY_SUFFIX)
            if os.path.exists(bv_program_path):
                shutil.rmtree(bv_program_path)
            os.mkdir(bv_program_path)
            self.repository_path = bv_program_path
            # check src folder, package and source code
            bv_source_path = os.path.join(bv_program_path, os.path.sep.join(JaidProperties.projectsourcedir),
                                          self.src_pkg)
            os.makedirs(bv_source_path)
            copyfile(o_source_path, os.path.join(bv_source_path, src_name))
            copyfile(o_weighted_edge_path, os.path.join(bv_source_path, self.WEIGHTEDEDGE_CLASS))
            copyfile(o_node_path, os.path.join(bv_source_path, self.NODE_CLASS))
            # check test folder, package and source code
            bv_test_path = os.path.join(bv_program_path, os.path.sep.join(JaidProperties.projecttestsourcedir),
                                        os.path.sep.join(self.test_pkg))
            os.makedirs(bv_test_path)
            copyfile(o_test_path, os.path.join(bv_test_path, test_name))
            copyfile(o_test_helper_path, os.path.join(bv_test_path, self.TEST_HELPER))
            # generate properties file

            jaid_properties = JaidProperties(self.exp_config, self.repository_path)
            libs = os.path.pathsep.join(self.get_qb_libs())
            jaid_properties.value_map[JaidProperties.p_ProjectLib] = libs

            jaid_properties.value_map[JaidProperties.p_MethodToFix] = self.__get_qb_mtf()
            self.set_models(jaid_properties, self.bv, '')
            bv_properties = jaid_properties.write_default_property_file()

            return bv_properties
        else:
            print("There is no QuixBugs class name: " + self.bv.upper())

    def run_bv(self):
        bv_properties = self.prepare_bv()
        if bv_properties:
            run_(self.exp_config, bv_properties)

    def read_bv(self):
        pass

    def get_repo_path(self):
        return os.path.join(self.exp_config.get_working_path(), constants.QUIX_BUGS_REPO_NAME)

    def get_qb_libs(self):
        repo_path = self.get_repo_path()
        lib_path = os.path.join(repo_path, "libs")
        libs = [os.path.join(lib_path, f) for f in os.listdir(lib_path) if os.path.isfile(os.path.join(lib_path, f))]
        return libs

    def __get_qb_mtf(self):
        qb_mtf_path = os.path.join(self.exp_config.get_lib_path(), self.QUIXBUGS_MTF_FILE)

        with open(qb_mtf_path) as mtf_file:
            mtfs = mtf_file.readlines()
            for line in mtfs:
                if line.startswith(self.class_name.upper() + ': '):
                    mtf = line[len(self.class_name + ': '):]
                    return mtf.strip()


class BearsBenchMark(AbsBenchMark):
    CLASSPATH_FILE_NAME = 'cp.txt'
    BUG_INFO_FILE_NAME = 'bears.json'
    MTF_FILE = 'bears_mtf.txt'
    DEFAULT_SRC = ['src/main/java', 'src', 'source']
    DEFAULT_TEST_SRC = ['src/test/java', 'test', 'tests']
    TARGET = ['target/classes', 'target', 'target/test-classes']

    def __init__(self, exp_config, bv):
        super().__init__(exp_config, bv)
        if 'Bears-' in bv:  # icj repo bug id
            self.bv = bv
            self.repository_path = os.path.join(exp_config.get_working_path(), bv)
            self.other_module_targets = ''

    def __checkout_repo(self):
        if not os.path.exists(self.repository_path):
            # Checkout buggy program
            cmd = "python2 %s/scripts/checkout_bug.py --bugId %s --workspace %s" % (
                self.exp_config.get_bears_exe(), self.bv, self.exp_config.get_working_path())
            print('CMD# ' + cmd)
            subprocess.call(cmd, shell=True)

        if os.path.exists(self.repository_path):
            # compile buggy program
            cmd = 'export JAVA_HOME=' + self.exp_config.get_java8() \
                  + "; python2 %s/scripts/compile_bug.py --bugId %s --workspace %s" % (
                      self.exp_config.get_bears_exe(), self.bv, self.exp_config.get_working_path())
            print('CMD# ' + cmd)
            subprocess.call(cmd, shell=True)

            # output classpath of buggy program
            prevdir = os.getcwd()
            os.chdir(self.repository_path)
            cmd = 'export JAVA_HOME=' + self.exp_config.get_java8() \
                  + '; mvn dependency:build-classpath -Dmdep.outputFile=cp.txt'
            print('CMD# ' + cmd)
            subprocess.call(cmd, shell=True)
            os.chdir(prevdir)
        else:
            print('Fail to checkout ' + self.bv)

    def __get_mtf(self):
        mtf_path = os.path.join(self.exp_config.get_lib_path(), self.MTF_FILE)
        with open(mtf_path) as mtf_file:
            mtfs = mtf_file.readlines()
            for line in mtfs:
                if line.startswith(self.bv + ': '):
                    mtf = line[len(self.bv + ': '):]
                    return mtf.strip()

    def __get_libs(self):
        libs = ''
        path = os.path.join(self.repository_path, self.CLASSPATH_FILE_NAME)
        if os.path.exists(path):
            with open(path, 'r') as f:
                lines = f.readlines()
                for l in lines:
                    libs += l + ':'
        for t in self.TARGET:
            if os.path.exists(os.path.join(self.repository_path, t)):
                libs += os.path.join(self.repository_path, t) + ':'
        libs = libs[:-1]
        libs += self.other_module_targets
        return libs

    def __get_dir(self, folders):
        for f in folders:
            if os.path.exists(os.path.join(self.repository_path, f)):
                return f

    def __get_other_module_target(self):
        for dirname, dirnames, filenames in os.walk(self.repository_path):
            for subdirname in dirnames:
                if subdirname in self.TARGET:
                    target_path = os.path.join(dirname, subdirname)
                    self.other_module_targets += ':' + target_path

    def prepare_bv(self):
        self.__checkout_repo()
        bug_info_f = os.path.join(self.repository_path, self.BUG_INFO_FILE_NAME)
        if os.path.exists(bug_info_f):
            with open(bug_info_f, 'r') as f:
                try:
                    bug_info = json.load(f)
                except Exception as e:
                    print("got %s on json.load()" % e)
                    sys.exit()
        failing_module = bug_info['tests']['failingModule']
        _, repo = os.path.split(failing_module)
        if not repo.isdigit():
            self.__get_other_module_target()
            self.repository_path = os.path.join(self.repository_path, repo)
            if not os.path.isdir(self.repository_path):
                print('FailingModule [' + failing_module + '] not exist!!')
                return 'None'

        jaid_properties = JaidProperties(self.exp_config, self.repository_path)
        jaid_properties.value_map[JaidProperties.p_ProjectLib] = self.__get_libs()
        jaid_properties.value_map[JaidProperties.p_MethodToFix] = self.__get_mtf()
        jaid_properties.value_map[JaidProperties.p_ProjectSourceDir] = self.__get_dir(self.DEFAULT_SRC)
        jaid_properties.value_map[JaidProperties.p_ProjectTestSourceDir] = self.__get_dir(self.DEFAULT_TEST_SRC)

        self.set_models(jaid_properties, self.bv, '')
        property_file_path = jaid_properties.write_default_property_file()
        print('The buggy program ' + repo + ' is ready. Program arguments of JAID running configuration is:')
        print(constants.JAID_ARG_PROPERTY + ' ' + property_file_path)
        return property_file_path

    def run_bv(self):
        properties_file_path = self.prepare_bv()
        run_(self.exp_config, properties_file_path)

    def read_bv(self):
        for inner_inner_folder in os.listdir(self.repository_path):
            if inner_inner_folder == constants.OUTPUT_FOLDER_NAME:  # find the experiment output in repo
                jaid_output_dir = os.path.join(self.repository_path, inner_inner_folder)
                read_new_jaid_result(jaid_output_dir)
