#! /usr/bin/env python3
import os
import platform
from collections import OrderedDict

from utils import get_all_files_recursively

JAID_JAR = 'jaid-all.jar'
ARG_PROPERTY = '--JaidSettingFile'

CLOUD_D4jDir = '/root/exp_tools/defects4j'
LOCAL_D4jDir = '/home/lschen/PycharmProjects/jaid_journal_exp/libs/defects4j'
CLOUD_MSR_D4jDir = '/root/test/msr_test/script/jaid_journal_exp/libs/defects4j'
CLOUD_MsrLibDir = '/root/test/msr_test/libs'
LOCAL_MsrLibDir = '/home/lschen/PycharmProjects/jaid_journal_exp/libs'

p_sep = '='
LINUX_PATH_SEP = '/'
LINUX_SEP = ':'
WIN_SEP = ';'

P_DEFAULT_LOGLEVEL = 'DEBUG'


class JaidProperties:
    p_JDKDir = 'JDKDir'
    p_LogLevel = 'LogLevel'
    p_ProjectRootDir = 'ProjectRootDir'
    p_ProjectSourceDir = 'ProjectSourceDir'
    p_ProjectOutputDir = 'ProjectOutputDir'
    p_ProjectLib = 'ProjectLib'
    p_ProjectTestSourceDir = 'ProjectTestSourceDir'
    p_ProjectTestOutputDir = 'ProjectTestOutputDir'
    p_ProjectTestsToInclude = 'ProjectTestsToInclude'
    p_ProjectTestsToExclude = 'ProjectTestsToExclude'
    p_ProjectExtraClasspath = 'ProjectExtraClasspath'
    p_ProjectCompilationCommand = 'ProjectCompilationCommand'
    p_ProjectExecutionCommand = 'ProjectExecutionCommand'
    p_MethodToFix = 'MethodToFix'
    p_Encoding = 'Encoding'
    p_ExpToExclude = 'ExpToExclude'
    p_TargetJavaVersion = 'TargetJavaVersion'
    p_SbflAlgorithm = 'SbflAlgorithm'
    p_RankingAlgorithm = 'RankingAlgorithm'
    p_MaxTestNumber = 'MaxTestNumber'
    p_EnableSecondValidation = 'EnableSecondValidation'
    p_DevFixFile = 'DevFixedFile'
    p_RankingModel = 'RankingModel'
    p_FeatureMap = 'FeatureMap'
    p_KilledModel = 'KilledModel'
    p_KilledFeatureMap = 'KilledFeatureMap'
    p_UpdateModel = 'UpdateModel'
    p_ModelPrepare = 'ModelPrepare'

    PROPERTY_FILE_NAME = 'fixja.properties'
    AN_PROPERTY_FILE_NAME = 'local_jaid.properties'
    projectsourcedir = ['src', 'main', 'java']
    projectoutputdir = ['out', 'production']
    projecttestsourcedir = ['src', 'test', 'java']
    projecttestoutputdir = ['out', 'test']

    def __init__(self, my_config, repository_path):
        self.exp_config = my_config

        self.value_map = OrderedDict([
            (self.p_JDKDir, my_config.JDKDir),
            (self.p_LogLevel, P_DEFAULT_LOGLEVEL),
            (self.p_ProjectRootDir, repository_path),
            (self.p_ProjectSourceDir, os.path.sep.join(self.projectsourcedir)),
            (self.p_ProjectTestSourceDir, os.path.sep.join(self.projecttestsourcedir)),
            (self.p_ProjectOutputDir, os.path.sep.join(self.projectoutputdir)),
            (self.p_ProjectTestOutputDir, os.path.sep.join(self.projecttestoutputdir)),
            (self.p_MethodToFix, ''),

            (self.p_ProjectLib, ''),
            (self.p_ProjectTestsToInclude, ''),
            (self.p_ProjectTestsToExclude, ''),
            (self.p_ProjectExtraClasspath, ''),
            (self.p_ProjectCompilationCommand, ''),
            (self.p_ProjectExecutionCommand, ''),
            (self.p_Encoding, ''),
            (self.p_ExpToExclude, ''),
            (self.p_TargetJavaVersion, ''),
            (self.p_SbflAlgorithm, my_config.SbflAlgorithm),
            (self.p_RankingAlgorithm, my_config.RankingAlgorithm),
            (self.p_MaxTestNumber, my_config.MaxTestNumber),
            (self.p_DevFixFile, ''),
            (self.p_EnableSecondValidation, 'false')])

    def __set_config(self):
        self.value_map[self.p_SbflAlgorithm] = self.exp_config.SbflAlgorithm
        self.value_map[self.p_RankingAlgorithm] = self.exp_config.RankingAlgorithm
        self.value_map[self.p_MaxTestNumber] = self.exp_config.MaxTestNumber

    def write_default_property_file(self):
        repository_path = os.path.abspath(self.value_map[self.p_ProjectRootDir])
        property_file_path = os.path.join(repository_path, self.AN_PROPERTY_FILE_NAME)
        content = ''
        prop_file = open(property_file_path, 'w')
        for key in self.value_map.keys():
            content += append_one_line_properties(key, self.value_map[key])

        prop_file.write(content)
        prop_file.close()
        return property_file_path

    def read_property_from_file(self, old_property_file_path):
        old_repository_path = None

        with open(old_property_file_path, 'r') as old_file:
            for line in old_file:
                if self.p_ProjectRootDir in line:
                    old_repository_path = line.split(p_sep)[1].strip()
                elif self.p_ProjectTestsToInclude in line:
                    old_test_to_include = line.split(p_sep)[1].strip()
                    self.value_map[self.p_ProjectTestsToInclude] = check_test_list(old_test_to_include)
                elif self.p_ProjectTestsToExclude in line:
                    old_test_to_exclude = line.split(p_sep)[1].strip()
                    self.value_map[self.p_ProjectTestsToExclude] = check_test_list(old_test_to_exclude)
                elif self.p_ProjectLib in line:
                    self.value_map[self.p_ProjectLib] = update_local_lib(line, self.exp_config,
                                                                         self.value_map[self.p_ProjectRootDir],
                                                                         old_repository_path=old_repository_path)

                else:
                    for key in self.value_map.keys():
                        if key in line and not key == self.p_LogLevel:
                            self.value_map[key] = line.split(p_sep)[1].strip()
        self.__set_config()


def update_local_lib(line, my_config, new_repository, old_repository_path=None):
    libs = line.split(p_sep)[1].split(os.pathsep)
    if len(libs) <= 1:
        libs = line.split(p_sep)[1].split(':')
    new_libs = []
    for lib in libs:
        lib = lib.strip()
        if 'junit-' in lib:
            lib = my_config.get_junit_path()
        elif old_repository_path and old_repository_path in lib:
            lib = os.path.relpath(lib, old_repository_path)
        elif CLOUD_D4jDir in lib:
            lib = os.path.join(my_config.get_d4j_path(), os.path.relpath(lib, CLOUD_D4jDir))
        elif CLOUD_MsrLibDir in lib:
            lib = os.path.join(my_config.get_lib_path(), os.path.relpath(lib, CLOUD_MsrLibDir))
        elif LOCAL_D4jDir in lib:
            lib = os.path.join(my_config.get_d4j_path(), os.path.relpath(lib, LOCAL_D4jDir))
        elif CLOUD_MSR_D4jDir in lib:
            lib = os.path.join(my_config.get_d4j_path(), os.path.relpath(lib, CLOUD_MSR_D4jDir))
        elif LOCAL_MsrLibDir in lib:
            lib = os.path.join(my_config.get_lib_path(), os.path.relpath(lib, LOCAL_MsrLibDir))
        elif lib.startswith('/tmp/'):
            lib = lib[len('/tmp/'):len(lib)]
            drive, repo_name = os.path.split(old_repository_path)
            # print(repo_name)
            # print(lib)
            lib = lib[len(repo_name) + 1:len(lib)]
        if not lib.startswith('/'):
            lib = os.path.join(new_repository, lib)
        if os.path.exists(lib):
            new_libs.append(lib)
        else:
            print('LIB NOT FOUND ::' + lib)
    has_hamcrest = False
    for a_lib in new_libs:
        if 'hamcrest' in a_lib:
            has_hamcrest = True
    if not has_hamcrest:
        new_libs.insert(0, os.path.join(my_config.get_lib_path(), 'hamcrest-core-1.3.jar'))

    new_libs = treatment_for_mockito(new_libs)
    # new_libs.extend(search_repo_libs(new_repository, libs))
    return os.pathsep.join(new_libs)


def search_repo_libs(repo_path, standard_libs):
    standard_lib_map={}
    for standard_lib in standard_libs:
        file = os.path.basename(standard_lib).strip()
        standard_lib_map[file]=standard_lib
    file_type = '**/*.jar'
    repo_jars = {}
    repo_jars.update(get_all_files_recursively(os.path.join(repo_path, 'lib'), file_type))
    repo_jars.update(get_all_files_recursively(os.path.join(repo_path, 'libs'), file_type))
    founded_jar = []
    for a_jar in repo_jars.keys():
        if a_jar not in standard_lib_map.keys():
            founded_jar.append(repo_jars[a_jar])
    return founded_jar


def treatment_for_mockito(new_libs):
    hamcrest_core_11 = False
    hamcrest_all_13 = False
    for a_lib in new_libs:
        if 'build/classes/main' in a_lib:
            new_libs.remove(a_lib)
        if 'hamcrest-core-1.1.jar' in a_lib:
            hamcrest_core_11 = True
        if 'hamcrest-all-1.3.jar' in a_lib:
            hamcrest_all_13 = True
    if hamcrest_core_11 and hamcrest_all_13:
        for a_lib in new_libs:
            if 'hamcrest-all-1.3.jar' in a_lib:
                new_libs.remove(a_lib)
    return new_libs


def check_path_format(line):
    if str(platform.system()).lower().startswith('windows'):
        if LINUX_PATH_SEP in line:
            return line.replace(LINUX_PATH_SEP, os.path.sep)
    return line


def path_normcase(line):
    content = line.split(p_sep)
    if len(content) > 1 and (LINUX_PATH_SEP in content[1] or os.path.sep in content[1]):
        path = os.path.normpath(content[1].strip())
        if str(platform.system()).lower().startswith('windows'):
            path = path.replace(os.path.sep, "\\\\")
        new_ = content[0] + p_sep + path + os.linesep
        # print('new_line: ' + new_)
        return new_
    return line


def check_test_list(test_list):
    if LINUX_SEP in test_list:
        tests = test_list.strip().split(LINUX_SEP)
        return os.pathsep.join(tests)
    elif WIN_SEP in test_list:
        tests = test_list.strip().split(WIN_SEP)
        return os.pathsep.join(tests)
    return test_list


def append_one_line_properties(pro_name, pro_val=""):
    return path_normcase(pro_name + p_sep + pro_val + os.linesep)
